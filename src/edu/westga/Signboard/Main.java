package edu.westga.Signboard;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.fxml.FXMLLoader;

/**
 * Starts the Signboard GUI.
 * 
 * @author Michael Morguarge
 * @version 1.0
 */
public class Main extends Application {
	
	/**
	 * Runs the program.
	 * 
	 * @precondition none.
	 * 
	 * @postcondition none.
	 * 
	 * @param args - not used.
	 */
	public static void main(String[] args) {
		launch(args);
	}

	/*
	 * (non-Javadoc)
	 * @see javafx.application.Application#start(javafx.stage.Stage)
	 */
	@Override
	public void start(Stage primaryStage) {
		try {
			BorderPane root = (BorderPane) FXMLLoader.load(getClass().getResource("/edu/westga/Signboard/view/signboardGUI.fxml"));
			Scene scene = new Scene(root, 850, 730);
			primaryStage.setResizable(false);
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch (Exception noShow) {
			noShow.printStackTrace();
		}
	}
}

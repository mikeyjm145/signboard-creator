/**
 * 
 */
package edu.westga.Signboard.io;

/**
 * Decides which action to take based on the value used
 * 
 * @author Michael Morguarge
 * @version 1.0
 */
public enum FileOperationType {
	/**
	 * Activates the open image file action for the file chooser
	 */
	Open_File,
	
	/**
	 * Activates the save/save as file action for the file chooser
	 */
	Save_File;
}
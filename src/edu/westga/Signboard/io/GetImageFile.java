/**
 * 
 */
package edu.westga.Signboard.io;

import java.io.File;

import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;

/**
 * The file chooser for the Signboard Gui
 * 
 * @author Michael Morguarge
 * @version 1.0
 */
public final class GetImageFile implements IOFileInterface {

	private File file;
	
	/**
	 * Creates a new file chooser
	 * 
	 * @precondition None
	 * @postcondition file == (user selected file)
	 * 
	 * @param operation - the operation to undertake
	 */
	public GetImageFile(FileOperationType operation) {
		
		if (operation == FileOperationType.Open_File) {
			this.file = this.uploadImageFile();
		} else if (operation == FileOperationType.Save_File) {
			this.file = this.saveImageFile();
		}
	}
	
	/*
	 * (non-Javadoc)
	 * @see edu.westga.Signboard.io.filechooserinterfaces.TheFileChooser#getFile()
	 */
	@Override
	public File getFile() {
		return this.file;
	}
	
	private File uploadImageFile() {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Upload Image File");
		fileChooser.getExtensionFilters().addAll(
						new ExtensionFilter("Image Files", "*.png", "*.jpg", "*.jpeg", "*.gif", "*.bmp"));
		File selectedFile = fileChooser.showOpenDialog(null);
		
		return selectedFile;
	}
	
	private File saveImageFile() {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Save Signboard Image");
		fileChooser.getExtensionFilters().addAll(
						new ExtensionFilter("PNG", "*.png"),
						new ExtensionFilter("JPG", "*.jpg"),
						new ExtensionFilter("JPEG", "*.jpeg"),
						new ExtensionFilter("GIF", "*.gif"),
						new ExtensionFilter("BMP", "*.bmp"));
		File selectedFile = fileChooser.showSaveDialog(null);
		
		return selectedFile;
	}
}

/**
 * 
 */
package edu.westga.Signboard.io;

import java.io.File;

/**
 * Definition for a file chooser object
 * 
 * @author Michael Morguarge
 * @version 1.0
 */
public interface IOFileInterface {

	/**
	 * Returns the file uploaded by the user.
	 * 
	 * @precondition None
	 * @postcondition None
	 * 
	 * @return the file uploaded by the user
	 */
	File getFile();
}

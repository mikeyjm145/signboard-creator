/**
 * 
 */
package edu.westga.Signboard.io;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import javax.activation.MimetypesFileTypeMap;

/**
 * Inspections for image files
 * 
 * @author Michael Morguarge
 * @version 1.0
 */
public class ImageFileInspection {

	/**
	 * Determines whether the file is an image
	 * 
	 * @precondition None
	 * @postcondition None
	 * 
	 * @param file - the file to be tested
	 * @return whether the file is an image or not
	 */
	public static final Boolean isAnImage(File file) {
		Boolean isImage = false;
		
		MimetypesFileTypeMap mimeType = new MimetypesFileTypeMap();
		mimeType.addMimeTypes("image png tif jpg jpeg bmp gif");
		
		String mimetype = mimeType.getContentType(file);
        String type = mimetype.split("/")[0];
        
        if (type.equals("image")) {
            isImage = true;
        }
        
        return isImage;
	}
	
	/**
	 * Returns the file extension of the image
	 * 
	 * @precondition None
	 * @postcondition None
	 * 
	 * @param file - the file to be checked
	 * @return the file extension of the image
	 */
	public static final String getFileExtension(File file) {
		String imageType = "";
		try {
			imageType = Files.probeContentType(file.toPath()).split("/")[1];
		} catch (IOException nullFileType) {
			nullFileType.printStackTrace();
		}
        
        return imageType;
	}
}

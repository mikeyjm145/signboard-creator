package edu.westga.Signboard.io;

import java.awt.image.BufferedImage;

import edu.westga.Signboard.model.Picture;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;

/**
 * Conversion class for images.
 * 
 * @author Michael Morguarge
 * @version 1.0
 */
public final class ImagePictureConversion {
	
	private ImagePictureConversion() {
		// To keep the class from being instantiated
	}

	/**
	 * Converts an Picture to an Image.
	 * 
	 * @precondition none.
	 * 
	 * @param picture - the picture to convert to a image
	 * 
	 * @return the image.
	 */
	public static Image convertPictureToImage(Picture picture) {
    	return SwingFXUtils.toFXImage(picture.getImage(), null);
    }
	
	/**
	 * Converts an Image to an Picture.
	 * 
	 * @precondition none.
	 * 
	 * @param image - the image to convert to a picture
	 * 
	 * @return the picture.
	 */
	public static Picture convertImageToPicture(Image image) {
    	BufferedImage theImage = SwingFXUtils.fromFXImage(image, null);
    	return new Picture("Converted image", theImage);
    }
}

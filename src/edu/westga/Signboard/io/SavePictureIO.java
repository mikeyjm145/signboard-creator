/**
 * 
 */
package edu.westga.Signboard.io;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

/**
 * Saves the signboard
 * 
 * @author Michael Morguarge
 * @version 1.0
 */
public final class SavePictureIO {

	private static SavePictureIO theInstance;
	
	private SavePictureIO() {
		// Keeps from creating more than one item
	}
	
	/**
	 * Saves the Signboard image to the specified location
	 * 
	 * @precondition image != null; file != null && typeOf(file) == image; saveType != null
	 * 
	 * @param image - the image to save
	 * @param file - the file to save it to
	 * 
	 * @return whether the image saved properly
	 * @throws IOException
	 *             IO exception thrown
	 */
	public boolean saveSignboardImage(BufferedImage image, File file) throws IOException {
		boolean isSaved = true;
		
		if (file == null || !ImageFileInspection.isAnImage(file)) {
			return !isSaved;
		}
		
		if (!file.exists()) {
			file = new File(file.getAbsolutePath());
		}
		
		BufferedImage theImage = new BufferedImage(image.getWidth(), image.getHeight(), BufferedImage.TYPE_INT_RGB);
		Graphics2D graphics = theImage.createGraphics();
		graphics.drawImage(image, 0, 0, null);
		graphics.dispose();
		
		isSaved = ImageIO.write(theImage, ImageFileInspection.getFileExtension(file), file);
		
		return isSaved;
	}
	
	/**
	 * Returns the single instance of this class.
	 * 
	 * @precondition	none
	 * @return			the instance
	 */
	public static SavePictureIO getInstance() {
		if (theInstance == null) {
			theInstance = new SavePictureIO();
		}
		return theInstance;
	}
}

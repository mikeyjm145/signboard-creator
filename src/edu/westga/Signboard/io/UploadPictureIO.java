/**
 * 
 */
package edu.westga.Signboard.io;

import java.awt.image.BufferedImage;

import java.io.File;
import java.io.IOException;

import java.net.URL;

import javax.imageio.ImageIO;

import edu.westga.Signboard.model.Picture;

/**
 * Handles the uploading, downloading and saving images
 * @author Michael Morguarge
 * @version 1.0
 */
public final class UploadPictureIO {
	private static final int DEFAULT_IMAGE_SIZE = 500;
	private static final String DEFAULT_IMAGE_NAME = "No Image";

	private static UploadPictureIO theInstance;
	
	private UploadPictureIO() {
		// Keeps from creating more than one item
	}
	
	/**
	 * Downloads an image from the internet.
	 * 
	 * @precondition imageURL != null
	 * @postcondition an Picture object is created.
	 * 
	 * @param imageURL - the URL of the online image.
	 * 
	 * @return the image downloaded from the internet
	 * 
	 * @exception IOException unable to download image file from web
	 */
	public Picture getImageFromWeblink(String imageURL) throws IOException {
		String name = "";
		BufferedImage image = null;
		
		if (imageURL.equals("") || !ImageFileInspection.isAnImage(new File(imageURL))) {
			name = "";
			image = new BufferedImage(1, 1, BufferedImage.TYPE_INT_RGB);
		} else {
			File file = new File(imageURL);
			name = file.getName();
			image = this.getImageFromURL(imageURL);
		}
		
		return this.createPicture(name, image);
	}
	
	/**
	 * Loads an image from the users computer.
	 * 
	 * @precondition none.
	 * 
	 * @param file - the uploaded file
	 * 
	 * @return the image loaded from the user computer.
	 * 
	 * @exception IOException unable to download image file from web
	 */
	public Picture loadImageFromLocalMachine(File file) throws IOException {
		String name = "";
		BufferedImage image = null;

		if (file == null || !ImageFileInspection.isAnImage(file)) {
			name = "";
			image = new BufferedImage(1, 1, BufferedImage.TYPE_INT_RGB);
		} else {
			name = file.getName();
			image = this.getImage(file);
		}
		
		return this.createPicture(name, image);
	}
	
	private BufferedImage getImageFromURL(String imageURL) throws IOException {
		BufferedImage image = null;
		
		URL url = new URL(imageURL);
		image = ImageIO.read(url);
		return image;
	}

	private BufferedImage getImage(final File file) throws IOException {
		BufferedImage image = null;
		image = ImageIO.read(file);
		
		return image;
	}

	private Picture createPicture(String name, BufferedImage image) {
		Picture picture = null;
		
		if (image == null) {
			picture = new Picture(DEFAULT_IMAGE_NAME, DEFAULT_IMAGE_SIZE, DEFAULT_IMAGE_SIZE);
		} else {
			picture = new Picture(name, image);
		}
		
		return picture;
	}
	
	/**
	 * Returns the single instance of this class.
	 * 
	 * @precondition	none
	 * @return			the instance
	 */
	public static UploadPictureIO getInstance() {
		if (theInstance == null) {
			theInstance = new UploadPictureIO();
		}
		return theInstance;
	}
}

package edu.westga.Signboard.model;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

/**
 * Picture represents a named image that can be
 * scaled in size.
 * 
 * @author CS 3211
 * @version Fall, 2014
 */
public final class Picture  {
	private final String name;
	private final BufferedImage image;
	private final int height;
	private final int width;
	
	
	/**
	 * Creates a new Picture with the specified name and image.
	 * 
	 * @precondition	aName != null && anImage != null
	 * @postcondition	getName.equals(name) && getImage().equals(anImage)
	 * 
	 * @param aName		the name of the image
	 * @param anImage	the image
	 */
	public Picture(final String aName, final BufferedImage anImage) {
		if (aName == null) {
			throw new IllegalArgumentException("Name can't be null");
		}
		if (anImage == null) {
			throw new IllegalArgumentException("Image can't be null");
		}
		if ((anImage.getWidth() <= 0) || (anImage.getHeight() <= 0)) {
			throw new IllegalArgumentException("Image can't have a width or length of 0");
		}
		
		this.name = aName;
		this.image = anImage;
		
		this.height = this.image.getHeight();
		this.width = this.image.getWidth();
	}
	
	/**
	 * Creates a new Picture with the specified name and an
	 * all-black image of the specified width and height.
	 * 
	 * @precondition	width > 0 && height > 0
	 * @postcondition	getWidth == width && getHeight == height && getName.equals(name)
	 * 
	 * @param aName		the name of the image
	 * @param width		the width of the image
	 * @param height	the height of the image
	 */
	public Picture(final String aName, final int width, final int height) {
		this(aName, new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB));
	}
	
	/**
	 * Returns this Picture's name.
	 * 
 	 * @precondition	none
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Returns this Picture's height.
	 * 
	 * @precondition	none
	 * @return the height
	 */
	public int getHeight() {
		return this.height;
	}

	/**
	 * Returns this Picture's width.
	 * 
	 * @precondition	none
	 * @return the width
	 */
	public int getWidth() {
		return this.width;
	}
	
	/**
	 * Returns this Picture's image.
	 * 
	 * @precondition	none
	 * @return			the image
	 */
	public BufferedImage getImage() {
		//TODO copy the image. Time how long it takes
		//search copy bufferedimage with java library and timing java code
		//http://www.javacodegeeks.com/2012/08/measure-execution-time-in-java-spring.html
		return this.createDeepCopyOfBufferedImage();
	}
	
	private BufferedImage createDeepCopyOfBufferedImage() {
	    BufferedImage bufferedImage = new BufferedImage(this.image.getWidth(), this.image.getHeight(), this.image.getType());
	    Graphics graphic = bufferedImage.getGraphics();
	    graphic.drawImage(this.image, 0, 0, null);
	    graphic.dispose();
	    return bufferedImage;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return this.name.hashCode() + this.height + this.width;
	}

	/**
	 * Returns true iff object to compare is a picture with the same name, width, height, width.
	 * 
	 * @return true if the other image is logically the same as this picture
	 * 
	 * @param objectToCompare the other object
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object objectToCompare) {
		if (this == objectToCompare) {
			return true;
		}
		if (objectToCompare == null) {
			return false;
		}
		if (getClass() != objectToCompare.getClass()) {
			return false;
		}
		Picture other = (Picture) objectToCompare;
		return (this.height == other.height) && (this.name.equals(other.name)) && (this.width == other.width);
	}
}

/**
 * 
 */
package edu.westga.Signboard.model.controller;

import javafx.scene.image.Image;

import edu.westga.Signboard.io.ImagePictureConversion;
import edu.westga.Signboard.model.Picture;
import edu.westga.Signboard.model.filters.ColorFilterType;
import edu.westga.Signboard.model.filters.StandardColorFilter;

/**
 * Controller for the image color filters
 * 
 * @author Michael Morguarge
 * @version 1.0
 */
public final class ImageColorFilterController {

	/**
	 * Constructs an controller that handles applying color filters to the image.
	 * 
	 * @precondition None
	 * 
	 * @postcondition None
	 */
	public ImageColorFilterController() {
		
	}
	
	/**
	 * Filters the image with the selected color filters
	 * 
	 * @precondition none
	 * 
	 * @param picture - the image to color filter
	 * @param redBoost - the red boost message
	 * @param greenBoost - the green boost message
	 * @param blueBoost - the blue boost message
	 * 
	 * @return a new color filtered picture
	 */
	public Image filterPicture(final Image picture, String redBoost, String greenBoost, String blueBoost) {
		Picture tempPicture = ImagePictureConversion.convertImageToPicture(picture);
		ColorFilterType filterPicture = new StandardColorFilter(tempPicture.getImage(), redBoost, greenBoost, blueBoost);
		
		Picture colorFilteredPicture = new Picture("Custom Filtered Image", filterPicture.getFilteredImage());
		Image image = ImagePictureConversion.convertPictureToImage(colorFilteredPicture);
		
		return image;
	}
}

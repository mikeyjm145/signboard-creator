/**
 * 
 */
package edu.westga.Signboard.model.controller;

import java.io.File;
import java.io.IOException;

import javafx.scene.image.Image;
import edu.westga.Signboard.io.ImagePictureConversion;
import edu.westga.Signboard.io.UploadPictureIO;
import edu.westga.Signboard.model.Picture;

/**
 * Controls the uplaod and download actions between the view and the model.
 * 
 * @author Michael Morguarge
 * @version 1.0
 */
public class ImageDisplayController {
	private String imageName;
	private UploadPictureIO io;

	/**
	 * Creates a Image display controller
	 * 
	 * @precondition None.
	 * @postcondition conversion = new ImagePictureConversion()
	 */
	public ImageDisplayController() {
		this.io = UploadPictureIO.getInstance();
	}
	
	/**
	 * Uploads an image from the users computer.
	 * 
	 * @precondition none.
	 * 
	 * @param file - the uploaded file
	 * 
	 * @return the image uploaded from the users computer.
	 * @throws IOException 
	 */
	public final Image uploadImage(File file) throws IOException {
		Image image = null;
		Picture thePicture = this.io.loadImageFromLocalMachine(file);
		this.imageName = thePicture.getName();
		
		if (thePicture.getImage().getHeight() != 1 && thePicture.getImage().getWidth() != 1) {
			image = ImagePictureConversion.convertPictureToImage(thePicture);
		}
		
		return image;
	}
	
	/**
	 * Downloads an image from the Internet.
	 * 
	 * @precondition webImageURL != null
	 * 
	 * @param webImageURL - the image's URL.
	 * 
	 * @return the image downloaded from the Internet.
	 * @throws IOException 
	 */
	public final Image downloadImage(String webImageURL) throws IOException {
		Image image = null;
		Picture thePicture = this.io.getImageFromWeblink(webImageURL);
		this.imageName = thePicture.getName();
		if (thePicture.getImage().getHeight() != 1 && thePicture.getImage().getWidth() != 1) {
			image = ImagePictureConversion.convertPictureToImage(thePicture);
		}
		return image;
	}
	
	/**
	 * Returns the name of the image. //TODO remove from class
	 * @return the name of the image
	 */
	public String getName() {
		return this.imageName;
	}
}

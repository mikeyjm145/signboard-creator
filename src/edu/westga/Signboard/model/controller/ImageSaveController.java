/**
 * 
 */
package edu.westga.Signboard.model.controller;

import java.io.File;
import java.io.IOException;

import javafx.scene.image.Image;

import edu.westga.Signboard.io.ImagePictureConversion;
import edu.westga.Signboard.io.SavePictureIO;

import edu.westga.Signboard.model.Picture;

/**
 * Controller for saving an image
 * 
 * @author Michael Morguarge
 * @version 1.0
 */
public final class ImageSaveController {

	private SavePictureIO io;
	
	/**
	 * Constructs an image save controller
	 * 
	 * @precondition None
	 * @postcondition io == instanceOfIO
	 */
	public ImageSaveController() {
		this.io = SavePictureIO.getInstance();
	}
	
	/**
	 * Saves the image to a file
	 * 
	 * @precondition None
	 * @postcondition None
	 * 
	 * @param theImage - the image to be saved
	 * @param file - the file to save the image to
	 * 
	 * @return whether the image was successfully saved
	 * 
	 * @throws IOException - Unable to save the file
	 */
	public boolean saveImage(Image theImage, File file) throws IOException {
		Picture thePicture = ImagePictureConversion.convertImageToPicture(theImage);
		return this.io.saveSignboardImage(thePicture.getImage(), file);
	}
}

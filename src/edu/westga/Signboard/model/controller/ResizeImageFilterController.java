package edu.westga.Signboard.model.controller;

import javafx.scene.image.Image;
import edu.westga.Signboard.io.ImagePictureConversion;
import edu.westga.Signboard.model.Picture;
import edu.westga.Signboard.model.filters.ResizeFilterType;
import edu.westga.Signboard.model.filters.ScaleToCustomSizeFilter;
import edu.westga.Signboard.model.filters.ScaleToHalfSizeFilter;

/**
 * Controls the image filtering between the view and the model class.
 * 
 * @author Michael Morguarge
 * @version 1.0
 */
public class ResizeImageFilterController {
	
	private Picture tempPicture;
	
	/**
	 * Constructs an controller that handles applying filters to the image.
	 * 
	 * @precondition picture != null.
	 * 
	 * @postcondition tempPicture = picture
	 * 
	 * @param picture - the image to filter.
	 */
	public ResizeImageFilterController(final Image picture) {
		this.tempPicture = ImagePictureConversion.convertImageToPicture(picture);
	}
	
	/**
	 * Transforms the image to be half its size.
	 * 
	 * @precondition none.
	 * 
	 * @return a new picture.
	 */
	public final Image resizePictureHalfSize() {
		ResizeFilterType imageFilter = new ScaleToHalfSizeFilter(this.tempPicture);
		Image image = ImagePictureConversion.convertPictureToImage(imageFilter.filterImage());
		return image;
	}
	
	/**
	 * Transforms the image using the custom size percentage.
	 * 
	 * @precondition none.
	 * 
	 * @param originalImageWidth
	 * @param originalImageHeight
	 * @param xScale - the x scale percentage.
	 * @param yScale - the y scale percentage.
	 * 
	 * @return a new picture.
	 */
	public final Image resizePictureCustomSize(double xScale, double yScale) {
		ResizeFilterType imageFilter = new ScaleToCustomSizeFilter(this.tempPicture, xScale, yScale);
		Image image = ImagePictureConversion.convertPictureToImage(imageFilter.filterImage());
		return image;
	}
}

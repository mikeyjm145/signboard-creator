package edu.westga.Signboard.model.controller;

import java.awt.image.BufferedImage;

import edu.westga.Signboard.model.signboardcreation.ImageParser;
import edu.westga.Signboard.model.signboardcreation.SignboardBuilder;

/**
 * Controls the creation of the signboard between the model class and the view class
 * @author Michael Morgaurge
 * @version 1.0
 */
public class SignboardCreationController {
	private ImageParser buildImage;
	
	/**
	 * Constructs an controller that handles creating a new Signboard Image.
	 * 
	 * @precondition None.
	 * @postcondition buildImage = new SignboardBuilder.
	 * 
	 * @param anImage - the image to convert to a image into a signboard
	 * @param pixelsPerWidth - the width of the sub-image
	 * @param pixelsPerHeight - the height of the sub-image
	 * @param signboardWidth - the custom width of the signboard
	 * @param signboardHeight - the custom height of the signboard
	 */
	public SignboardCreationController(final BufferedImage anImage, int pixelsPerWidth, final int pixelsPerHeight, int signboardWidth, int signboardHeight) {
		this.buildImage = new SignboardBuilder(anImage, pixelsPerWidth, pixelsPerHeight, signboardWidth, signboardHeight);
	}
	
	/**
	 * Returns new the Signboard image
	 * 
	 * @precondition None.
	 * @postcondition None.
	 * 
	 * @return the new Signboard image
	 */
	public final BufferedImage getSignboardImage() {
		return this.buildImage.getSignboardImage();
	}
}

/**
 * 
 */
package edu.westga.Signboard.model.filters;

import java.awt.image.BufferedImage;

/**
 * Definition of the behavior of a Color Filter
 * 
 * @author Michael Morguarge
 * @version 1.0
 *
 */
public interface ColorFilterType {
	
	/**
	 * Returns the color filtered buffered image
	 * 
	 * @precondition None
	 * @postcondition None
	 * 
	 * @return the color filtered buffered image
	 */
	BufferedImage getFilteredImage();

}

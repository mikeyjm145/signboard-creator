/**
 * 
 */
package edu.westga.Signboard.model.filters;

import edu.westga.Signboard.model.Picture;

/**
 * Default definition of a filter.
 * 
 * @author Michael MOrguarge
 * @version 1.0
 */
public abstract class ResizeFilterType {
	private Picture thePictureToEdit;
	
	/**
	 * Creates the filter to edit the image.
	 * 
	 * @precondition: None.
	 * @postcondition: None.
	 * 
	 * @param thePicture - the image to be edited.
	 */
	public ResizeFilterType(Picture thePicture) {
		this.thePictureToEdit = thePicture;
	}
	
	/**
	 * Filters/Modifies the image with the selected filter.
	 * 
	 * @precondition: None.
	 * @postcondition: the image is modified.
	 * 
	 * @return The modified buffered image
	 */
	public abstract Picture filterImage();
	
	/**
	 * Returns the image to edit.
	 * 
	 * @precondition: None.
	 * @postcondition: The image has been modified.
	 * 
	 * @return The modified image
	 */
	protected final Picture getImageToEdit() {
		return this.thePictureToEdit;
	}
	
}

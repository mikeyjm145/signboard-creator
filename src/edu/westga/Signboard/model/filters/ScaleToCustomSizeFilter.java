package edu.westga.Signboard.model.filters;

import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;

import edu.westga.Signboard.model.Picture;

/**
 * Creates a custom scaling filter
 * 
 * @author Micahel Morguarge
 * @version 1.0
 */
public class ScaleToCustomSizeFilter extends ResizeFilterType {
	private final double scaleX;
	private final double scaleY;

	/**
	 * Creates a standard filter to edit the image.
	 * 
	 * @precondition:  0 < scaleX && 0 < scaleY
	 * 				   0 < originalWidth &&  0 < originalHeight 
	 * @postcondition: _scaleX = scaleX && _scaleY = scaleY
	 * 				   _originalWidth = originalWidth && _originalHeight = originalHeight
	 * 
	 * @param thePicture - the image to scale
	 * @param scaleX - the x scale percentage.
	 * @param scaleY - the y scale percentage.
	 * @param originalWidth
	 * @param originalHeight
	 */
	public ScaleToCustomSizeFilter(Picture thePicture, double scaleX, double scaleY) {
		super(thePicture);
		
		if (this.scaleX < 0 || this.scaleY < 0) {
			throw new IllegalArgumentException("Scale values must be greater than 0");
		}
		
		this.scaleX = scaleX;
		this.scaleY = scaleY;
	}

	/* (non-Javadoc)
	 * @see edu.westga.pictureeditor.filterinterface.FilterType#filterImage(edu.westga.pictureeditor.model.Picture)
	 */
	@Override
	public final Picture filterImage() {
		return this.scaleImage();
	}

	private Picture scaleImage() {
		AffineTransform transformation =
			    AffineTransform.getScaleInstance(this.scaleX,
			    								 this.scaleY);
		AffineTransformOp transformationOperation =  new AffineTransformOp(
						transformation, AffineTransformOp.TYPE_BILINEAR);
		BufferedImage newImage = transformationOperation.filter(this.getImageToEdit().getImage(), null);
		return new Picture(this.getImageToEdit().getName(), newImage);
	}
}

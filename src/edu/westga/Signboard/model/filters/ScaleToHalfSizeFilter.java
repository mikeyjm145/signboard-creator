/**
 * 
 */
package edu.westga.Signboard.model.filters;

import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;

import edu.westga.Signboard.model.Picture;

/**
 * Creates a standard scaling filter
 * 
 * @author Micahel Morguarge
 * @version 1.0
 */
public class ScaleToHalfSizeFilter extends ResizeFilterType {
	private static final double SCALING_FACTOR = 0.5;

	/**
	 * Creates a standard filter to edit the image.
	 * 
	 * @precondition: None.
	 * @postcondition: None.
	 *
	 * @param thePicture - the image.
	 */
	public ScaleToHalfSizeFilter(Picture thePicture) {
		super(thePicture);
	}

	/* (non-Javadoc)
	 * @see edu.westga.pictureeditor.filterinterface.FilterType#filterImage(edu.westga.pictureeditor.model.Picture)
	 */
	@Override
	public final Picture filterImage() {
		return this.scaleImage();
	}

	private Picture scaleImage() {
		AffineTransform transformation =
			    AffineTransform.getScaleInstance(SCALING_FACTOR,
			    								 SCALING_FACTOR);
		AffineTransformOp transormationOperation = 
						new AffineTransformOp(transformation, AffineTransformOp.TYPE_BILINEAR);
		BufferedImage newImage = transormationOperation.filter(this.getImageToEdit().getImage(), null);
		return new Picture(this.getImageToEdit().getName(), newImage);
	}
}

/**
 * 
 */
package edu.westga.Signboard.model.filters;

import java.awt.Color;
import java.awt.image.BufferedImage;

/**
 * Filters an image by increasing the red byte value
 * 
 * @author Michael Morguarge
 * @version 1.0
 */
public class StandardColorFilter implements ColorFilterType {
	
	private static final int BOOST_BY_10 = 10;
	private static final int BOOST_BY_25 = 25;
	private static final int BOOST_BY_50 = 50;
	private static final int COLOR_LIMIT = 255;
	
	private final int boostRed;
	private final int boostGreen;
	private final int boostBlue;
	
	private final BufferedImage colorBoostedImage;
	
	/**
	 * Creates a Standard Color Filter
	 * 
	 * @precondition theImage != null
	 * 					&& boostRed != null
	 * 					&& boostGreen != null
	 * 					&& boostBlue != null
	 * 
	 * @param theImage - the image to apply the color filter to
	 * @param boostRed - the message to increase red
	 * @param boostGreen - the message to increase green
	 * @param boostBlue - the message to increase blue
	 */
	public StandardColorFilter(BufferedImage theImage, String boostRed, String boostGreen, String boostBlue) {
		if ((theImage == null) || (boostRed == null) || (boostGreen == null) || (boostBlue == null)) {
			throw new IllegalArgumentException("the image or boostRed/Green/Blue is null");
		}
		
		this.boostRed = this.colorBoost(boostRed);
		this.boostGreen = this.colorBoost(boostGreen);
		this.boostBlue = this.colorBoost(boostBlue);
		
		this.colorBoostedImage = this.filterImage(theImage);
	}

	/*
	 * (non-Javadoc)
	 * @see edu.westga.Signboard.model.filters.ColorFilterType#getFilterImage()
	 */
	@Override
	public BufferedImage getFilteredImage() {
		return this.colorBoostedImage;
	}
	
	private BufferedImage filterImage(BufferedImage theImage) {
		BufferedImage image = new BufferedImage(theImage.getWidth(), theImage.getHeight(), BufferedImage.TYPE_INT_RGB);
		
		for (int x = 0; x < theImage.getWidth(); x++) {
			for (int y = 0; y < theImage.getHeight(); y++) {
				
				Color color = new Color(theImage.getRGB(x, y));
				
				int rgb = this.boostColor(color);
				
				image.setRGB(x, y, rgb);
			}
		}
		return image;
	}
	
	private int boostColor(Color color) {
		int red = color.getRed();
		int green = color.getGreen();
		int blue = color.getBlue();
		
		if (this.colorWithinRange(red, this.boostRed)) {
			red = red + this.boostRed;
		}
		
		if (this.colorWithinRange(green, this.boostGreen)) {
			green = green + this.boostGreen;
		}
		
		if (this.colorWithinRange(blue, this.boostBlue)) {
			blue = blue + this.boostBlue;
		}

		Color aColor = new Color(red, green, blue);
		return aColor.getRGB();
	}
	
	private boolean colorWithinRange(int colorValue, int boost) {
		return (colorValue < (COLOR_LIMIT - boost + 1));
	}

	private int colorBoost(String boost) {
		int colorBoost = 0;
		
		if (boost.equals("Boost By 10")) {
			colorBoost = BOOST_BY_10;
		} else if (boost.equals("Boost By 25")) {
			colorBoost = BOOST_BY_25;
		} else if (boost.equals("Boost By 50")) {
			colorBoost = BOOST_BY_50;
		}
		
		return colorBoost;
	}
}

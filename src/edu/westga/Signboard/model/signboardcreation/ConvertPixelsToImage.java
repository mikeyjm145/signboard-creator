/**
 * 
 */
package edu.westga.Signboard.model.signboardcreation;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

/**
 * Converts a list of pixels into an image
 * 
 * @author Michael Morguarge
 * @version 1.0
 */
public class ConvertPixelsToImage {
	private final BufferedImage imageCreatedFromListOfPixels;
	
	/**
	 * Converts a list of pixels into an image
	 * 
	 * @precondition imageWidth > 0 && imageHeight > 0
	 * @postcondition None
	 * 
	 * @param theOrginialImageColors - the original image colors
	 * @param imageWidth - the original image width
	 * @param imageHeight - the orignial image height
	 */
	public ConvertPixelsToImage(ArrayList<Color> theOrginialImageColors, int imageWidth, int imageHeight) {
		if (imageWidth <= 0 || imageHeight <= 0) {
			throw new IllegalArgumentException("Image width or length must be greater than 0.");
		}
		
		this.imageCreatedFromListOfPixels = this.buildOriginalImage(theOrginialImageColors, imageWidth, imageHeight);
	}
	
	/**
	 * Returns the image converted from a list of pixels
	 * 
	 * @precondition None
	 * @postcondition None
	 * 
	 * @return the image converted from a list of pixels
	 */
	public BufferedImage getConvertedImage() {
		return this.imageCreatedFromListOfPixels;
	}
	
	private BufferedImage buildOriginalImage(ArrayList<Color> colors, int imageWidth, int imageHeight) {
		BufferedImage originalImage = new BufferedImage(imageWidth, imageHeight, BufferedImage.TYPE_INT_RGB);
		
		int colorIndex = 0;
		for (int y = 0; y < originalImage.getHeight(); y++) {
			for (int x = 0; x < originalImage.getWidth(); x++) {
				Color color = colors.get(colorIndex);
				originalImage.setRGB(x, y, color.getRGB());
				colorIndex++;
			}
		}
		
		return originalImage;
	}
}

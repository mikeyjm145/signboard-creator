/**
 * 
 */
package edu.westga.Signboard.model.signboardcreation;

import java.awt.image.BufferedImage;

/**
 * Default definition of a ImageBuilder
 * 
 * @author Michael Morguarge
 * @version 1.0
 */
public interface ImageParser {
	
	/**
	 * Returns the signboard image (BufferedImage)
	 * 
	 * @return the buffered image
	 */
	BufferedImage getSignboardImage();
}

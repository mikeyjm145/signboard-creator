/**
 * 
 */
package edu.westga.Signboard.model.signboardcreation;

import java.awt.Color;
import java.awt.image.BufferedImage;

import edu.westga.Signboard.model.Picture;

/**
 * Builds the signboard image.
 * 
 * @author Michael Morguarge
 * @version 1.0
 */
public class SignboardBuilder implements ImageParser {
	private final BufferedImage signboardImage;
	//private BufferedImage pixelizedImage;
	
	private final int subImageWidth;
	private final int subImageHeight;
	
	private final SignboardPixelFactory thePixel;
	
	/**
	 * Creates a new Signboard image with the passed in image
	 * 
	 * @precondition signboardWidth > 0 && signboardHeight > 0 && subWidth > 0 && subHeight > 0
	 * @postcondition signboardImage = new Signboard Image
	 * 					&& resizedPixelHolder = new SignboardPixelHolder
	 * 					&& originalSignboard = image
	 * 
	 * @param image - the image to be converted to a signboard
	 * @param subWidth - the width of the sub-image
	 * @param subHeight - the height of the sub-image
	 * @param signboardWidth - the width of the signboard image
	 * @param signboardHeight - the height of the signboard image
	 */
	public SignboardBuilder(BufferedImage image, int subWidth, int subHeight, int signboardWidth, int signboardHeight) {
		if (subWidth < 1 || subHeight < 1) {
			throw new IllegalArgumentException(
					"SubWidth or SubHeight cannot be 0 or negative.");
		}
		
		if (signboardWidth < 0 && signboardHeight < 0) {			
			throw new IllegalArgumentException(
					"Signboard Width or Signboard Height cannot be 0 or negative.");
		}
		
		this.subImageWidth = subWidth;
		this.subImageHeight = subHeight;
		this.thePixel = new SignboardPixelFactory();
		
		if (signboardWidth > 1 && signboardHeight > 1) {
			this.signboardImage = this.pixelateSignboardImage(image);
		} else {
			BufferedImage theImage = this.copyImage(image);
			this.signboardImage = this.pixelateSignboardImage(theImage);
		}
	}
	
	/**
	 * Returns the signboard image
	 * 
	 * @precondition None
	 * @postcondition None
	 * 
	 * @return the signboard image
	 */
	public final BufferedImage getSignboardImage() {
		return this.signboardImage;
	}
	
	private  BufferedImage copyImage(BufferedImage image) {
		Picture copyOfOriginalImage = new Picture("theImage", image);

		return copyOfOriginalImage.getImage();
	}
	
	private BufferedImage pixelateSignboardImage(BufferedImage anImage) {
		BufferedImage pixelizedImage = anImage;
		for (int y = 0; y < pixelizedImage.getHeight(); y += this.subImageHeight) {
			for (int x = 0; x < pixelizedImage.getWidth(); x += this.subImageWidth) {
				this.pixelateImage(pixelizedImage, x, y);
			}
		}
		
		return pixelizedImage;
	}

	private void pixelateImage(BufferedImage pixelizedImage, int x, int y) {
		int[] subimageDimensions = {this.subImageWidth, this.subImageHeight};
		Color pixel = this.thePixel.createPixel(pixelizedImage, x, y, subimageDimensions);
		
		for (int subY = y; subY < (y + this.subImageHeight) && (subY < pixelizedImage.getHeight()); subY++) {
			for (int subX = x; subX < (x + this.subImageWidth) && (subX < pixelizedImage.getWidth()); subX++) {
				pixelizedImage.setRGB(subX, subY, pixel.getRGB());
			}
		}
	}
}

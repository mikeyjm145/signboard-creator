/**
 * 
 */
package edu.westga.Signboard.model.signboardcreation;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import edu.westga.Signboard.model.Picture;

/**
 * Builds the signboard image using a list of pixels.
 * 
 * @author Michael Morguarge
 * @version 1.0
 */
public class SignboardBuilderUsingPixelList implements ImageParser {
	private final BufferedImage signboardImage;
	private BufferedImage pixelizedImage;
	
	private final int subImageWidth;
	private final int subImageHeight;
	
	private final SignboardPixelFactory thePixel;
	
	/**
	 * Creates a new Signboard image with a list of pixels
	 * 
	 * @precondition None
	 * @postcondition None
	 * 
	 * @param theOrginialImageColors - the list of image colors
	 * @param imageWidth - the original image width
	 * @param imageHeight - the orignial image height
	 * @param subWidth - the sub image width
	 * @param subHeight - the sub image height
	 * @param signboardWidth - the signboard width
	 * @param signboardHeight - the signboard height
	 */
	public SignboardBuilderUsingPixelList(ArrayList<Color> theOrginialImageColors, int imageWidth, int imageHeight, int subWidth, int subHeight, int signboardWidth, int signboardHeight) {
		if (imageWidth < 1 || imageHeight < 1) {
			throw new IllegalArgumentException(
					"ImageWidth or ImageHeight cannot be 0 or negative.");
		}
		
		if (subWidth < 1 || subHeight < 1) {
			throw new IllegalArgumentException(
					"SubWidth or SubHeight cannot be 0 or negative.");
		}
		
		if (signboardWidth < 0 && signboardHeight < 0) {			
			throw new IllegalArgumentException(
					"Signboard Width or Signboard Height cannot be 0 or negative.");
		}
		
		ConvertPixelsToImage convertPixels = new ConvertPixelsToImage(theOrginialImageColors, imageWidth, imageHeight);
		
		BufferedImage image = convertPixels.getConvertedImage();
		
		this.subImageWidth = subWidth;
		this.subImageHeight = subHeight;
		this.thePixel = new SignboardPixelFactory();
		
		if (signboardWidth > 1 && signboardHeight > 1) {
			this.signboardImage = this.pixelateSignboardImage(image);
		} else {
			BufferedImage theImage = this.copyImage(image);
			this.signboardImage = this.pixelateSignboardImage(theImage);
		}
	}
	
	/**
	 * Returns the signboard image
	 * 
	 * @precondition None
	 * @postcondition None
	 * 
	 * @return the signboard image
	 */
	public final BufferedImage getSignboardImage() {
		return this.signboardImage;
	}
	
	private  BufferedImage copyImage(BufferedImage image) {
		Picture copyOfOriginalImage = new Picture("theImage", image);

		return copyOfOriginalImage.getImage();
	}
	
	private BufferedImage pixelateSignboardImage(BufferedImage anImage) {
		this.pixelizedImage = anImage;
		for (int y = 0; y < this.pixelizedImage.getHeight(); y += this.subImageHeight) {
			for (int x = 0; x < this.pixelizedImage.getWidth(); x += this.subImageWidth) {
				this.pixelateImage(x, y);
			}
		}
		
		return this.pixelizedImage;
	}

	private void pixelateImage(int x, int y) {
		int[] subimageDimensions = {this.subImageWidth, this.subImageHeight};
		Color pixel = this.thePixel.createPixel(this.pixelizedImage, x, y, subimageDimensions);
		
		for (int subY = y; subY < (y + this.subImageHeight) && (subY < this.pixelizedImage.getHeight()); subY++) {
			for (int subX = x; subX < (x + this.subImageWidth) && (subX < this.pixelizedImage.getWidth()); subX++) {
				this.pixelizedImage.setRGB(subX, subY, pixel.getRGB());
			}
		}
	}
}

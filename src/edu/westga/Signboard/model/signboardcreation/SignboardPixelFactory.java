/**
 * 
 */
package edu.westga.Signboard.model.signboardcreation;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

/**
 * Creates one signboard pixel from a sub-image of a BufferedImage
 * 
 * @author Michael Morguarge
 * @version 1.0
 *
 */
public class SignboardPixelFactory {
	private int redSum;
	private int blueSum;
	private int greenSum;
	
	/**
	 * Creates a Signboard pixel
	 * 
	 * @precondition None
	 * @postcondition redSum == 0
	 * 					&& blueSum == 0
	 * 					&& greenSum == 0
	 */
	public SignboardPixelFactory() {
		this.redSum = 0;
		this.blueSum = 0;
		this.greenSum = 0;
	}
	
	/**
	 * Returns the average color of the sub image
	 * 
	 * @precondition none.
	 * 
	 * @param subImage - the sub image.
	 * 
	 * @return the average color of the sub image
	 */
	public Color getAverageColorFromSubimage(final BufferedImage subImage) {
		if (subImage == null) {
			throw new IllegalArgumentException("anImage cannot be null.");
		}
		
		return this.getAverageColors(subImage);
	}
	
	/**
	 * Converts an image to a list of pixles
	 * 
	 * @precondition anImage != null
	 * 					&& subWidth > 0
	 * 					&& subHeight > 0
	 * 
	 * @postcondition None
	 * 
	 * @param anImage - the image to convert to a list of pixels
	 * @param subWidth - the width of the sub images
	 * @param subHeight - the height of the sub images
	 * 
	 * @return the list of pixels
	 */
	public ArrayList<Color> convertImageToPixels(BufferedImage anImage, int subWidth, int subHeight) {
		ArrayList<Color> colors = new ArrayList<Color>();
		
		if (anImage == null) {
			throw new IllegalArgumentException("anImage cannot be null.");
		}
		if (subWidth < 0 || subHeight < 0) {
			throw new IllegalArgumentException("subWidth or subHeight must be greater than 0.");
		}
		
		int[] subimageDimensions = {subWidth, subHeight};
		
		for (int y = 0; y < anImage.getHeight(); y += subHeight) {
			for (int x = 0; x < anImage.getWidth(); x += subWidth) {
				Color pixel = this.createPixels(anImage, x, y, subimageDimensions);
				colors.add(pixel);
			}
		}
		return colors;
	}
	
	/**
	 * Creates the average color from the sub image starting from the (x, y) to (x + subWidth, y + subHeight)
	 * 
	 * @precondition None
	 * @postcondition None
	 * 
	 * @param anImage - the image to retrieve the sub image from
	 * @param x - the x position start of the pixel
	 * @param y - the y position start of the pixel
	 * @param subImageDimensions - the sub image dimensions
	 * 
	 * @return the average color of the block of pixels
	 */
	public Color createPixel(BufferedImage anImage, int x, int y, int[] subImageDimensions) {
		return this.createPixels(anImage, x, y, subImageDimensions);
	}
	
	private Color createPixels(BufferedImage anImage, int x, int y, int[] subImageDimensions) {
		int subWidth = subImageDimensions[0];
		int subHeight = subImageDimensions[1];
		
		if (x + subWidth > anImage.getWidth()) {
			subWidth = Math.abs(x - anImage.getWidth());
		}
		
		if (y + subHeight > anImage.getHeight()) {
			subHeight = Math.abs(y - anImage.getHeight());
		}
		
		BufferedImage subImage = anImage.getSubimage(x, y,
						subWidth, subHeight);
		
		return this.getAverageColors(subImage);
	}
	
	private Color getAverageColors(final BufferedImage subImage) {
		this.resetColorSums();
		this.getPixels(subImage);
		
		return this.getAverageRGBValue(subImage);
	}

	private void getPixels(final BufferedImage subImage) {
		for (int row = 0; row < subImage.getWidth(); row++) {
			for (int col = 0; col < subImage.getHeight(); col++) {
				int rgbValue = subImage.getRGB(row, col);
				
				Color pixelColor = new Color(rgbValue);
				
				this.incrementColorSum(pixelColor);
			}
		}
	}
	
	private void resetColorSums() {
		this.redSum = 0;
		this.greenSum = 0;
		this.blueSum = 0;
	}
	
	private void incrementColorSum(Color aColor) {
		this.redSum += aColor.getRed();
		this.greenSum += aColor.getGreen();
		this.blueSum += aColor.getBlue();
	}
	
	private Color getAverageRGBValue(final BufferedImage subImage) {
		int imageHeight = subImage.getHeight();
		int imageWidth = subImage.getWidth();
		
		int redColorAverage = this.redSum / (imageWidth * imageHeight);
		int greenColorAvareage = this.greenSum / (imageWidth * imageHeight);
		int blueColorAverage = this.blueSum / (imageWidth * imageHeight);
		
		return new Color(redColorAverage, greenColorAvareage, blueColorAverage);
	}
}

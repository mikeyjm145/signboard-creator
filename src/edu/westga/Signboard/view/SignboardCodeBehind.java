package edu.westga.Signboard.view;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import edu.westga.Signboard.viewmodel.SignboardViewModel;
import javafx.application.Platform;

//import javafx.collections.ObservableList;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SingleSelectionModel;
import javafx.scene.control.Slider;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;

/**
 * Controller for the Signboard GUI.
 * 
 * @author Michael Morguarge
 * @version 1.0
 */
public class SignboardCodeBehind implements Initializable {
	private static final int DEFAULT_CHOICE = 0;
	private static final int EDITED_IMAGE_TAB = 1;
	private static final int PREVIEW_IMAGE_TAB = 2;

	@FXML
	private Label pixelsPerWidth;

	@FXML
	private Slider pixelsPerWidthSlider;

	@FXML
	private ImageView editedImage;

	@FXML
	private MenuItem saveSignboardMenuItem;

	@FXML
	private MenuItem aboutMenuItem;

	@FXML
	private Slider pixelsPerHeightSlider;

	@FXML
	private ComboBox<String> redFilterComboBox;

	@FXML
	private Button createSignboardButton;

	@FXML
	private TextField originalImageSize;

	@FXML
	private Label resizeHeightPercentage;

	@FXML
	private Button downloadImageButton;

	@FXML
	private MenuItem exitMenuItem;

	@FXML
	private MenuItem resizeImageMenuItem;

	@FXML
	private MenuItem createSignboardMenuItem;

	@FXML
	private ImageView originalImage;

	@FXML
	private TextField imageLinkBox;

	@FXML
	private MenuItem resetImageMenuItem;

	@FXML
	private TextField signboardWidth;

	@FXML
	private TextField imageName;

	@FXML
	private TextField editedImageSize;

	@FXML
	private Slider imageHeightSlider;

	@FXML
	private Button resetImageButton;

	@FXML
	private TextField signboardHeight;

	@FXML
	private Slider imageWidthSlider;

	@FXML
	private ComboBox<String> blueFilterComboBox;

	@FXML
	private TabPane theImageDisplayer;

	@FXML
	private Button uploadImageButton;

	@FXML
	private ComboBox<String> greenFilterComboBox;

	@FXML
	private MenuItem uploadFromPCMenuItem;

	@FXML
	private Label resizeWidthPercentage;

	@FXML
	private Button resizeImageButton;

	@FXML
	private MenuItem downloadFromWebMenuItem;

	@FXML
	private Button applyColorFilterButton;

	@FXML
	private Label pixelsPerHeight;

	@FXML
	private Button saveSignboardButton;

	@FXML
	private Label displayNameOfImage;
	
    @FXML
    private ImageView originalImageSidePreview;
    
    @FXML
    private ImageView editedImageSidePreview;

	// private ObservableList<Picture> imageHistory;
	private SignboardViewModel signboardVM;

	/*
	 * (non-Javadoc)
	 * 
	 * @see javafx.fxml.Initializable#initialize(java.net.URL,
	 * java.util.ResourceBundle)
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		this.signboardVM = new SignboardViewModel();

		this.linkResetSignboardEventsAndValues();
		
		this.linkPixelizationEventsAndValues();

		this.linkImageDownloadEventsAndValues();

		this.linkSignboardCreationEventsAndValues();

		this.linkImageDisplayEventsAndValues();

		this.linkImageInformationEventsAndValues();

		this.linkImageResizeEventsAndValues();

		this.linkColorFilterEventsAndValues();
		
		this.linkSaveImageEventsAndValues();
		
		this.linkUploadImageEventsAndValues();

		this.linkCloseMenuItemEventsAndValues();
	}
	
	private void linkColorFilterEventsAndValues() {
		this.redFilterComboBox.valueProperty()
						.bindBidirectional(this.signboardVM.linkRedFilterSelectionProperty());
		this.redFilterComboBox.getItems().addAll(this.signboardVM.getFilterOptions());
		
		this.greenFilterComboBox.valueProperty()
						.bindBidirectional(this.signboardVM.linkGreenFilterSelectionProperty());
		this.greenFilterComboBox.getItems().addAll(this.signboardVM.getFilterOptions());
		
		this.blueFilterComboBox.valueProperty()
						.bindBidirectional(this.signboardVM.linkBlueFilterSelectionProperty());
		this.blueFilterComboBox.getItems().addAll(this.signboardVM.getFilterOptions());
		
		this.applyColorFilterButton.disableProperty().bind(
						this.signboardVM.linkSignboardTabDisabled());
		
		this.applyColorFilterButton.setOnAction((event) -> {
				this.signboardVM.applyFilters();
		});
	}
	
	private void linkUploadImageEventsAndValues() {
		this.uploadImageButton.setOnAction((event) -> {
				this.uploadImageFromComputer();
		});
		
		this.uploadFromPCMenuItem.setOnAction((event) -> {
				this.uploadImageFromComputer();
		});
	}
	
	private void linkSaveImageEventsAndValues() {
		this.saveSignboardButton.disableProperty().bind(
						this.signboardVM.linkSignboardTabDisabled());

		this.saveSignboardMenuItem.disableProperty().bind(
						this.signboardVM.linkSignboardTabDisabled());
		
		this.saveSignboardButton.setOnAction((event) -> {
				this.saveSignboard();
		});
	
		this.saveSignboardMenuItem.setOnAction((event) -> {
				this.saveSignboard();
		});
	}

	private void linkPixelizationEventsAndValues() {
		this.pixelsPerWidthSlider.valueProperty().bindBidirectional(this.signboardVM.linkPixelsPerWidthProperty());
		this.pixelsPerWidthSlider.setValue(2);

		this.pixelsPerHeightSlider.valueProperty().bindBidirectional(this.signboardVM.linkPixelsPerHeightProperty());
		this.pixelsPerHeightSlider.setValue(2);

		this.pixelsPerWidth.textProperty().bindBidirectional(this.signboardVM.linkPixelsPerWidthLabelProperty());
		this.pixelsPerWidth.setText("2");

		this.pixelsPerHeight.textProperty().bindBidirectional(this.signboardVM.linkPixelsPerHeightLabelProperty());
		this.pixelsPerHeight.setText("2");
	
		this.createSignboardButton.disableProperty().bind(
						this.signboardVM.linkSignboardTabDisabled());

		this.createSignboardMenuItem.disableProperty().bind(
						this.signboardVM.linkSignboardTabDisabled());
		
		this.createSignboardButton.setOnAction((event) -> {
				this.convertToSignboard(DEFAULT_CHOICE);
		});
	
		this.createSignboardMenuItem.setOnAction((event) -> {
				int choice = this.signboardVM.getPixelizationInformation();
				this.convertToSignboard(choice);
		});
	}

	private void linkImageDownloadEventsAndValues() {
		this.downloadImageButton.disableProperty().bind(this.signboardVM.downloadPossible());

		this.imageLinkBox.textProperty().bindBidirectional(this.signboardVM.linkDownloadImageTextAreaProperty());
		
		this.downloadImageButton.setOnAction((event) -> {
				this.downloadImageFromWeb();
		});
	
		this.downloadFromWebMenuItem.setOnAction((event) -> {
				this.signboardVM.getDownloadInformation("Download Image From Web", "Please enter web address for image");
				this.downloadImageFromWeb();
		});
	}

	private void linkSignboardCreationEventsAndValues() {
		this.createSignboardButton.disableProperty().bind(this.signboardVM.signboardCreationPossible());
		
		this.signboardWidth.textProperty().bindBidirectional(this.signboardVM.linkSignboardWidth());

		this.signboardHeight.textProperty().bindBidirectional(this.signboardVM.linkSignboardHeight());
	}

	private void linkImageDisplayEventsAndValues() {
		Image image = this.originalImage.getImage();
		this.originalImage.imageProperty()
				.bindBidirectional(this.signboardVM.linkOriginalImageProperty());
		this.originalImageSidePreview.imageProperty()
				.bindBidirectional(this.signboardVM.linkOriginalImageProperty());
		this.originalImage.setImage(image);

		this.editedImage.imageProperty()
				.bindBidirectional(this.signboardVM.linkEditedImageProperty());
		this.editedImageSidePreview.imageProperty()
				.bindBidirectional(this.signboardVM.linkEditedImageProperty());
		this.editedImage.setImage(image);
		
		this.theImageDisplayer.getTabs().get(EDITED_IMAGE_TAB)
				.disableProperty()
				.bind(this.signboardVM.linkSignboardTabDisabled());
		
		this.theImageDisplayer.getTabs().get(PREVIEW_IMAGE_TAB)
				.disableProperty()
				.bind(this.signboardVM.linkSignboardTabDisabled());
	}

	private void linkImageInformationEventsAndValues() {
		String imageName = this.displayNameOfImage.getText();
		this.displayNameOfImage.textProperty().bindBidirectional(this.signboardVM.linkImageNameProperty());
		this.displayNameOfImage.setText(imageName);
		
		this.imageName.textProperty().bindBidirectional(this.signboardVM.linkImageNameProperty());
		this.originalImageSize.textProperty().bindBidirectional(this.signboardVM.linkOriginalImageSizeProperty());
		this.editedImageSize.textProperty().bindBidirectional(this.signboardVM.linkEditedImageSizeProperty());
	}

	private void linkImageResizeEventsAndValues() {
		this.imageWidthSlider.valueProperty()
				.bindBidirectional(this.signboardVM.linkWidthPercentageProperty());
		this.imageHeightSlider.valueProperty()
				.bindBidirectional(this.signboardVM.linkHeightPercentageProperty());
		
		this.resizeWidthPercentage.textProperty()
				.bindBidirectional(this.signboardVM.linkResizeWidthPercentageLabelProperty());
		this.resizeHeightPercentage.textProperty()
				.bindBidirectional(this.signboardVM.linkResizeHeightPercentageLabelProperty());
		
		this.resizeImageButton.disableProperty().bind(
						this.signboardVM.linkSignboardTabDisabled());

		this.resizeImageMenuItem.disableProperty().bind(
						this.signboardVM.linkSignboardTabDisabled());
		
		this.resizeImageButton.setOnAction((event) -> {
				this.resizeImage(DEFAULT_CHOICE);
		});
	
		this.resizeImageMenuItem.setOnAction((event) -> {
				int choice = this.signboardVM.getResizeInformation();
				this.resizeImage(choice);
		});
	}

	private void linkResetSignboardEventsAndValues() {
		this.resetImageButton.disableProperty().bind(
						this.signboardVM.linkSignboardTabDisabled());

		this.resetImageMenuItem.disableProperty().bind(
						this.signboardVM.linkSignboardTabDisabled());
		
		this.resetImageButton.setOnAction((event) -> {
				this.resetImage();
		});
	
		this.resetImageMenuItem.setOnAction((event) -> {
				this.resetImage();
		});
	}

	private void linkCloseMenuItemEventsAndValues() {
		this.exitMenuItem.setOnAction((event) -> {
				Platform.exit();
		});
	}

	@FXML
	private void changeImageHeightZoom(MouseEvent event) {
		this.setValueOfLabel(this.imageHeightSlider, this.resizeHeightPercentage);
	}

	@FXML
	private void changeImageWidthZoom(MouseEvent event) {
		this.setValueOfLabel(this.imageWidthSlider, this.resizeWidthPercentage);
	}

	@FXML
	private void changePixelWidthPerSquare(MouseEvent event) {
		this.setValueOfLabel(this.pixelsPerWidthSlider, this.pixelsPerWidth);
	}

	@FXML
	private void changePixelHeightPerSquare(MouseEvent event) {
		this.setValueOfLabel(this.pixelsPerHeightSlider, this.pixelsPerHeight);
	}

	// TODO: Regular Private Helper Methods
	private void resetImage() {
		int choice = this.signboardVM.replaceImageDialog(
						"Reset Signboard Image",
						"Are you sure you want to reset the signboard image?");
		
		if (choice == DEFAULT_CHOICE) {
			this.signboardVM.resetTheImage();
			
			this.editedImage.setFitWidth(this.originalImage.getFitWidth());
			this.editedImage.setFitHeight(this.originalImage.getFitHeight());
		}
	}

	private void resizeImage(int yesToResize) {
		if (yesToResize == DEFAULT_CHOICE) {
			this.signboardVM.resizeImage();
		}
	}

	private void convertToSignboard(int userChoice) {
		boolean isImageConverted = this.signboardVM.convertToSignboardImage();
		boolean isSignboardTabAvailable = !this.signboardVM.linkSignboardTabDisabled().getValue();
		
		this.editedImage.setFitWidth(this.editedImage.getFitWidth());
		this.editedImage.setFitHeight(this.editedImage.getFitHeight());

		if (isImageConverted && isSignboardTabAvailable && userChoice == DEFAULT_CHOICE) {
			this.selectTab(EDITED_IMAGE_TAB);
		} else {
			this.signboardVM.showMessage("Unable to convert to signboard",
							"Bad value entered for signboard image width or signboard image height. Please try again.");
		}
	}

	private void uploadImageFromComputer() {
		try {
			boolean isImageUploaded = this.signboardVM.uploadImageFromComputer();
			boolean isSignboardTabAvailable = !this.signboardVM
							.linkSignboardTabDisabled().getValue();

			if (isImageUploaded && isSignboardTabAvailable) {
				this.selectTab(EDITED_IMAGE_TAB);
			} else {
				this.signboardVM.showMessage("No Image Found",
								"Please try to upload your image again.");
			}

		} catch (IOException | NullPointerException e1) {
			this.signboardVM.showMessage("No Image Found",
							"Please try to upload your image again.");
		}
	}

	private void downloadImageFromWeb() {
		try {

			boolean isImageDownloaded = this.signboardVM.downloadImageFromWeb();
			boolean isSignboardTabAvailable = !this.signboardVM
							.linkSignboardTabDisabled().getValue();

			if (isImageDownloaded && isSignboardTabAvailable) {
				this.selectTab(EDITED_IMAGE_TAB);
			} else {
				this.signboardVM.showMessage("No Image Found",
								"Please try to download your image again.");
			}

		} catch (IOException nullImage) {
			this.signboardVM.showMessage("No Image Found",
							"Please try to download your image again.");
		}
	}

	private void saveSignboard() {
		try {
			boolean isImageSaved = this.signboardVM.saveSignboard();

			if (isImageSaved) {
				this.signboardVM.showMessage("Saved",
								"The image successfully saved to your computer.");
			} else {
				this.signboardVM.showMessage("Unable to save",
								"Please try to save your image again.");
			}
		} catch (Exception badSave) {
			badSave.printStackTrace();
			this.signboardVM.showMessage("Unable to Save",
							"Please try to save your image again.");
		}
	}

	private void setValueOfLabel(Slider theSlider, Label theLabel) {
		theSlider.valueChangingProperty().addListener(this.signboardVM.getChangeListener(theSlider, theLabel));
	}

	private void selectTab(final int theTab) {
		SingleSelectionModel<Tab> signboardTabs = this.theImageDisplayer
						.getSelectionModel();
		signboardTabs.select(theTab);
	}
}

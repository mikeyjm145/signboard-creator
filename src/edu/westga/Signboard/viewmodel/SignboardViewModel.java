package edu.westga.Signboard.viewmodel;

import java.io.IOException;
import java.util.Hashtable;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JSlider;
import javax.swing.JTextField;

import edu.westga.Signboard.io.FileOperationType;
import edu.westga.Signboard.io.GetImageFile;
import edu.westga.Signboard.io.ImagePictureConversion;
import edu.westga.Signboard.model.Picture;
import edu.westga.Signboard.model.controller.ImageColorFilterController;
import edu.westga.Signboard.model.controller.ImageDisplayController;
import edu.westga.Signboard.model.controller.ImageSaveController;
import edu.westga.Signboard.model.controller.ResizeImageFilterController;
import edu.westga.Signboard.model.controller.SignboardCreationController;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyBooleanProperty;
import javafx.beans.property.ReadOnlyBooleanWrapper;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.image.Image;

/**
 * Model for the SignboardViewCodeBehind
 * 
 * @author Michael Morguarge
 * @version 1.0
 */
public class SignboardViewModel {
	private static String zero = "0";
	private static String one = "1";
	private static String two = "2";
	private static String fifty = "50";
	private static String eighty = "80";
	private static String oneHundred = "100";
	
	private static int integerZero = 0;
	private static int integerOne = 1;
	private static int integerTwo = 2;
	private static int integerFifty = 50;
	private static int integerOneHundred = 100;
	
	private static double doubleOneHundred = 100.0;
	
	private final ReadOnlyBooleanWrapper downloadPossible = new ReadOnlyBooleanWrapper();
	private final ReadOnlyBooleanWrapper signboardTabDisabled = new ReadOnlyBooleanWrapper();
	private final ReadOnlyBooleanWrapper signboardCreationPossible = new ReadOnlyBooleanWrapper();

	private final SimpleStringProperty downloadImageTextAreaProperty = new SimpleStringProperty();
	private final SimpleStringProperty editedImageSizeProperty = new SimpleStringProperty();
	private final SimpleStringProperty originalImageSizeProperty = new SimpleStringProperty();
	private final SimpleStringProperty imageNameProperty = new SimpleStringProperty();
	private final SimpleStringProperty signboardWidthProperty = new SimpleStringProperty();
	private final SimpleStringProperty signboardHeightProperty = new SimpleStringProperty();
	private final SimpleStringProperty pixelsPerWidthLabelProperty = new SimpleStringProperty();
	private final SimpleStringProperty pixelsPerHeightLabelProperty = new SimpleStringProperty();
	private final SimpleStringProperty resizeWidthPercentageLabelProperty = new SimpleStringProperty();
	private final SimpleStringProperty resizeHeightPercentageLabelProperty = new SimpleStringProperty();
	private final SimpleStringProperty selectedRedFilterProperty = new SimpleStringProperty();
	private final SimpleStringProperty selectedGreenFilterProperty = new SimpleStringProperty();
	private final SimpleStringProperty selectedBlueFilterProperty = new SimpleStringProperty();

	private final SimpleIntegerProperty pixelsPerHeightProperty = new SimpleIntegerProperty();
	private final SimpleIntegerProperty pixelsPerWidthProperty = new SimpleIntegerProperty();

	private final SimpleDoubleProperty heightPercentageProperty = new SimpleDoubleProperty();
	private final SimpleDoubleProperty widthPercentageProperty = new SimpleDoubleProperty();

	private final ObjectProperty<Image> originalImageToDisplay = new SimpleObjectProperty<Image>();
	private final ObjectProperty<Image> editedImageToDisplay = new SimpleObjectProperty<Image>();

	private ResizeImageFilterController resizeFilterController;
	private ImageDisplayController displayImage;
	private SignboardCreationController createSignboard;
	private ImageSaveController saveImage;
	private ImageColorFilterController colorFilter;

	/**
	 * Creates the ViewModel for the SignboardViewCodeBehind
	 * 
	 * @precondition None
	 * @postcondition - downloadPossible <binded to> downloadTextAreaProperty.isNotEmpty
	 *                signboardCreationPossible <binded to> signboardWidthProperty.isNotEmpty,
	 *                		signboardHeightProperty.isNotEmpty,
	 *                		signboardTabDisabled
	 *                && signboardTabDisabled.getValue == true
	 *                && signboardWidthPercentagevalue.getValue == "0"
	 *                && signboardHeightPercentagevalue.getValue == "0"
	 *                && widthPercentageProperty.getValue == 1
	 *                && resizeWidthPercentageLabelProperty.getValue == "1"
	 *                && heightPercentageProperty.getValue == 1
	 *                && resizeHeightPercentageLabelProperty.getValue == "1"
	 *                && pixelsPerWidthProperty.getValue == 1
	 *                && pixelsPerWidthLabelProperty.getValue == "1"
	 *                && pixelsPerHeightProperty.getValue == 1
	 *                && pixelsPerHeightLabelProperty.getValue == "1"
	 *                && saveTypeProperty.getValue == "jpeg"
	 *                && selectedRedFilter.getValue == "No Filter"
	 *                && selectedBlueFilter.getValue == "No Filter"
	 *                && selectedGreenFilter.getValue == "No Filter"
	 */
	public SignboardViewModel() {
		this.displayImage = new ImageDisplayController();

		this.saveImage = new ImageSaveController();

		this.downloadPossible.bind(this.downloadImageTextAreaProperty
						.isNotEmpty().not());

		this.signboardCreationPossible.bind((this.signboardWidthProperty
						.isNotEmpty().and(this.signboardHeightProperty.isNotEmpty()
						.and(this.signboardWidthProperty.isNotEqualTo(zero)
						.and(this.signboardHeightProperty.isNotEqualTo(zero)))))
						.not().and(this.signboardTabDisabled));

		this.signboardTabDisabled.set(true);
		this.signboardWidthProperty.setValue(zero);
		this.signboardHeightProperty.setValue(zero);

		this.widthPercentageProperty.setValue(integerOneHundred);
		this.resizeWidthPercentageLabelProperty.setValue(oneHundred);

		this.heightPercentageProperty.setValue(integerOneHundred);
		this.resizeHeightPercentageLabelProperty.setValue(oneHundred);

		this.pixelsPerWidthProperty.setValue(integerTwo);
		this.pixelsPerWidthLabelProperty.setValue(two);

		this.pixelsPerHeightProperty.setValue(integerTwo);
		this.pixelsPerHeightLabelProperty.setValue(two);
		
		this.selectedRedFilterProperty.setValue("No Filter");
		this.selectedGreenFilterProperty.setValue("No Filter");
		this.selectedBlueFilterProperty.setValue("No Filter");
	}

	/**
	 * Returns a boolean property that decides whether an image download is
	 * possible.
	 * 
	 * @precondition None
	 * @postcondition None
	 * 
	 * @return boolean value that decides whether an image download is possible.
	 */
	public ReadOnlyBooleanProperty downloadPossible() {
		return this.downloadPossible;
	}

	/**
	 * Returns a boolean property that enables and disables the Signboard tab.
	 * 
	 * @precondition None
	 * @postcondition None
	 * 
	 * @return boolean value that enables and disables the Signboard tab.
	 */
	public ReadOnlyBooleanProperty linkSignboardTabDisabled() {
		return this.signboardTabDisabled;
	}

	/**
	 * Returns whether the user is able to create a signboard image
	 * 
	 * @precondition None
	 * @postcondition None
	 * 
	 * @return whether the user is able to create a signboard image
	 */
	public ReadOnlyBooleanProperty signboardCreationPossible() {
		return this.signboardCreationPossible;
	}

	/**
	 * Returns the download image dimension area property
	 * 
	 * @precondition None
	 * @postcondition None
	 * 
	 * @return the download image dimension area property
	 */
	public StringProperty linkDownloadImageTextAreaProperty() {
		return this.downloadImageTextAreaProperty;
	}

	/**
	 * Returns the original image name area property
	 * 
	 * @precondition None
	 * @postcondition None
	 * 
	 * @return the download image name area property
	 */
	public StringProperty linkImageNameProperty() {
		return this.imageNameProperty;
	}

	/**
	 * Returns the original image dimension area property
	 * 
	 * @precondition None
	 * @postcondition None
	 * 
	 * @return the original image dimension area property
	 */
	public StringProperty linkOriginalImageSizeProperty() {
		return this.originalImageSizeProperty;
	}

	/**
	 * Returns the original image dimension area property
	 * 
	 * @precondition None
	 * @postcondition None
	 * 
	 * @return the original image dimension area property
	 */
	public StringProperty linkEditedImageSizeProperty() {
		return this.editedImageSizeProperty;
	}

	/**
	 * Returns the custom signboard width property
	 * 
	 * @precondition None
	 * @postcondition None
	 * 
	 * @return the custom signboard width property
	 */
	public StringProperty linkSignboardWidth() {
		return this.signboardWidthProperty;
	}

	/**
	 * Returns the custom signboard height property
	 * 
	 * @precondition None
	 * @postcondition None
	 * 
	 * @return the custom signboard height property
	 */
	public StringProperty linkSignboardHeight() {
		return this.signboardHeightProperty;
	}

	/**
	 * Returns the pixels per width label property
	 * 
	 * @precondition None
	 * @postcondition None
	 * 
	 * @return the pixels per width label property
	 */
	public StringProperty linkPixelsPerWidthLabelProperty() {
		return this.pixelsPerWidthLabelProperty;
	}

	/**
	 * Returns the pixels per height label property
	 * 
	 * @precondition None
	 * @postcondition None
	 * 
	 * @return the pixels per height label property
	 */
	public StringProperty linkPixelsPerHeightLabelProperty() {
		return this.pixelsPerHeightLabelProperty;
	}

	/**
	 * Returns the resized width percentage label property
	 * 
	 * @precondition None
	 * @postcondition None
	 * 
	 * @return the resized width percentage label property
	 */
	public StringProperty linkResizeWidthPercentageLabelProperty() {
		return this.resizeWidthPercentageLabelProperty;
	}

	/**
	 * Returns the resized height percentage label property
	 * 
	 * @precondition None
	 * @postcondition None
	 * 
	 * @return the resized height percentage label property
	 */
	public StringProperty linkResizeHeightPercentageLabelProperty() {
		return this.resizeHeightPercentageLabelProperty;
	}

	/**
	 * Returns the red filter combo box string property
	 * 
	 * @precondition None
	 * @postcondition None
	 * 
	 * @return the red filter combo box string property
	 */
	public StringProperty linkRedFilterSelectionProperty() {
		return this.selectedRedFilterProperty;
	}

	/**
	 * Returns the green filter combo box string property
	 * 
	 * @precondition None
	 * @postcondition None
	 * 
	 * @return the green filter combo box string property
	 */
	public StringProperty linkGreenFilterSelectionProperty() {
		return this.selectedGreenFilterProperty;
	}

	/**
	 * Returns the blue filter combo box string property
	 * 
	 * @precondition None
	 * @postcondition None
	 * 
	 * @return the blue filter combo box string property
	 */
	public StringProperty linkBlueFilterSelectionProperty() {
		return this.selectedBlueFilterProperty;
	}

	/**
	 * Returns the pixel per width integer property
	 * 
	 * @precondition None
	 * @postcondition None
	 * 
	 * @return the pixel per width integer property
	 */
	public IntegerProperty linkPixelsPerWidthProperty() {
		return this.pixelsPerWidthProperty;
	}

	/**
	 * Returns the pixels per height integer property
	 * 
	 * @precondition None
	 * @postcondition None
	 * 
	 * @return the pixels per height integer property
	 */
	public IntegerProperty linkPixelsPerHeightProperty() {
		return this.pixelsPerHeightProperty;
	}

	/**
	 * Returns the width change percentage property
	 * 
	 * @precondition None
	 * @postcondition None
	 * 
	 * @return the width change percentage property
	 */
	public DoubleProperty linkWidthPercentageProperty() {
		return this.widthPercentageProperty;
	}

	/**
	 * Returns the height change percentage property
	 * 
	 * @precondition None
	 * @postcondition None
	 * 
	 * @return the the height change percentage property
	 */
	public DoubleProperty linkHeightPercentageProperty() {
		return this.heightPercentageProperty;
	}

	/**
	 * Returns the original image property
	 * 
	 * @precondition None
	 * @postcondition None
	 * 
	 * @return the original image property
	 */
	public ObjectProperty<Image> linkOriginalImageProperty() {
		return this.originalImageToDisplay;
	}

	/**
	 * Returns the edited image property
	 * 
	 * @precondition None
	 * @postcondition None
	 * 
	 * @return the edited image property
	 */
	public ObjectProperty<Image> linkEditedImageProperty() {
		return this.editedImageToDisplay;
	}

	/**
	 * Returns the list of options the user can select for adding a color filter
	 * to an image
	 * 
	 * @precondition None
	 * @postcondition None
	 * 
	 * @return the list of options the user can select for adding a color filter
	 *         to an image
	 */
	public final ObservableList<String> getFilterOptions() {
		return (ObservableList<String>) FXCollections.observableArrayList(
				"No Filter", "Boost by 10", "Boost By 25", "Boost By 50");
	}

	/**
	 * Applies the filters on the signboard image
	 * 
	 * @precondition None
	 * @postcondition None
	 */
	public void applyFilters() {
		String redBoost = this.selectedRedFilterProperty.getValue();
		String greenBoost = this.selectedGreenFilterProperty.getValue();
		String blueBoost = this.selectedBlueFilterProperty.getValue();
		
		Image editedImage = this.editedImageToDisplay.getValue();

		this.colorFilter = new ImageColorFilterController();
		Image coloBoostedImage = this.colorFilter.filterPicture(editedImage, redBoost, greenBoost, blueBoost);
		
		this.editedImageToDisplay.setValue(coloBoostedImage);
	}

	/**
	 * Resizes the image based on the slider set percentage value
	 * 
	 * @precondition None
	 * @postcondition The edited signboard image will change to the newly
	 *                resized edited signboard image
	 */
	public void resizeImage() {
		double realWidthPercentageValue = this.widthPercentageProperty.getValue() / doubleOneHundred;
		double realHeightPercentageValue = this.heightPercentageProperty.getValue() / doubleOneHundred;
		Image originalImage = this.originalImageToDisplay.getValue();
		
		Image theResizedImage = this.resize(originalImage, realWidthPercentageValue, realHeightPercentageValue);
		this.editedImageToDisplay.setValue(theResizedImage);

		this.setOriginalImageInformation();
		this.setEditedImageInformation();
	}
	
	private Image resize(Image image, double widthPercentage, double heightPercentage) {
		this.resizeFilterController = new ResizeImageFilterController(
				image);

		Image theResizedImage = this.resizeFilterController.resizePictureCustomSize(
						widthPercentage, heightPercentage);
		
		return theResizedImage;
	}

	/**
	 * Uploads an image from the computer
	 * 
	 * @precondition None
	 * @postcondition originalImageToDisplay.getvalue == uploaded image &&
	 *                editedImageToDisplay == uploaded image
	 * 
	 * @return whether the image uploaded properly
	 * 
	 * @throws IOException
	 *             - uploaded image == null || an IO error occurred
	 */
	public boolean uploadImageFromComputer() throws IOException {
		boolean isUploaded = true;

		GetImageFile openFileChooser = new GetImageFile(FileOperationType.Open_File);

		Image theUploadedImage = this.displayImage.uploadImage(openFileChooser.getFile());

		if (theUploadedImage != null) {
			this.originalImageToDisplay.setValue(theUploadedImage);
			this.editedImageToDisplay.setValue(theUploadedImage);

			this.setOriginalImageInformation();
			this.setEditedImageInformation();

			this.signboardTabDisabled.setValue(false);
		} else {
			return !isUploaded;
		}

		return isUploaded;
	}

	/**
	 * Downloads an image from the web
	 * 
	 * @precondition None
	 * @postcondition originalImageToDisplay.getvalue == downloaded image &&
	 *                editedImageToDisplay == downloaded image
	 * 
	 * @return whether the image downloaded properly
	 * 
	 * @throws IOException
	 *             downloaded image == null || an IO error occurred
	 */
	public boolean downloadImageFromWeb() throws IOException {
		boolean imageIsDownloaded = true;

		Image theDownloadedImage = this.displayImage
						.downloadImage(this.downloadImageTextAreaProperty.getValueSafe());

		if (theDownloadedImage != null) {
			this.originalImageToDisplay.setValue(theDownloadedImage);
			this.editedImageToDisplay.setValue(theDownloadedImage);

			this.setOriginalImageInformation();
			this.setEditedImageInformation();
			this.downloadImageTextAreaProperty.setValue("");
			this.signboardTabDisabled.setValue(false);
		} else {
			return !imageIsDownloaded;
		}

		return imageIsDownloaded;
	}

	/**
	 * Save the Signboard image
	 * 
	 * @precondition None
	 * 
	 * @return whether the signboard saved properly
	 * @throws IOException
	 *             IO exception occurred
	 */
	public boolean saveSignboard() throws IOException {
		GetImageFile saveFileChooser = new GetImageFile(FileOperationType.Save_File);
		Image theImageToSave = this.editedImageToDisplay.getValue();

		return this.saveImage.saveImage(theImageToSave, saveFileChooser.getFile());
	}

	private void setOriginalImageInformation() {
		this.imageNameProperty.setValue(this.displayImage.getName());

		int origWidth = (int) this.originalImageToDisplay.getValue().getWidth();
		int origHeight = (int) this.originalImageToDisplay.getValue().getHeight();

		this.originalImageSizeProperty.setValue(origWidth + " x " + origHeight);
	}

	private void setEditedImageInformation() {
		int editedWidth = (int) this.editedImageToDisplay.getValue().getWidth();
		int editedHeight = (int) this.editedImageToDisplay.getValue().getHeight();

		this.editedImageSizeProperty.setValue(
						editedWidth + " x " + editedHeight);
	}

	/**
	 * Resets the edited image back to the original image as a signboard
	 * 
	 * @precondition None
	 * @postcondition editedImageToDisplay.getImage ==
	 *                originalImageToDisplay.getImage -iff-
	 *                signboardTabDisabled.get == false
	 */
	public void resetTheImage() {
		if (!this.signboardTabDisabled.getValue()) {
			this.editedImageToDisplay.setValue(this.originalImageToDisplay.getValue());
			this.setEditedImageInformation();
		}
	}

	/**
	 * Converts an image into a signboard
	 * 
	 * @precondition None
	 * @postcondition editedImageToDisplay.getValue != editedImageToDisplay.getImage @previous -iff-
	 *                signboardTabDisabled.getValue == false
	 *                
	 * @return whether the signboard converted into an image
	 */
	public boolean convertToSignboardImage() {
		boolean isConverted = true;
		
		try {
			int signboardWidth = Integer.parseInt(
							this.signboardWidthProperty.getValue());
	
			int signboardHeight = Integer.parseInt(
							this.signboardHeightProperty.getValue());
	
			int subImageWidth = (int) this.pixelsPerWidthProperty.getValue();
	
			int subImageHeight = (int) this.pixelsPerHeightProperty.getValue();
	
			boolean canConvertImage = this.signboardConversionPossible(signboardWidth,
							signboardHeight, subImageWidth, subImageHeight);
			
			if (!this.signboardCreationPossible.getValue()
							&& !this.imageNameProperty.getValue().equals("Original Image")
							&& canConvertImage) {
				this.convertToSignboard(signboardWidth, signboardHeight, subImageWidth, subImageHeight);
			} else {
				throw new NumberFormatException("Returning false");
			}
		} catch (NumberFormatException badNumber) {
			return !isConverted;
		}
		

		return isConverted;
	}

	private boolean signboardConversionPossible(int signboardWidth,
					int signboardHeight, int subImageWidth, int subImageHeight) {
		boolean canConvertImage = true;

		if ((signboardWidth < integerZero) || (signboardHeight < integerZero)) {
			canConvertImage = false;

		} else if ((signboardWidth >= this.originalImageToDisplay.getValue().getWidth())
						|| (signboardHeight >= this.originalImageToDisplay.getValue().getHeight())) {
			canConvertImage = false;

		} else if ((subImageWidth <= integerZero) || (subImageHeight <= integerZero)) {
			canConvertImage = false;
			
		}

		return canConvertImage;
	}

	private void convertToSignboard(int signboardWidth, int signboardHeight, int subImageWidth, int subImageHeight) {
		Image resizedImage = this.originalImageToDisplay.getValue();
		
		if (signboardWidth > integerZero && signboardHeight > integerZero) {
			double imageWidth = this.originalImageToDisplay.getValue().getWidth();
			double imageHeight = this.originalImageToDisplay.getValue().getHeight();
			
			double widthPercentage = signboardWidth / imageWidth;
			double heightPercentage = signboardHeight / imageHeight;
			resizedImage = this.resize(resizedImage, widthPercentage, heightPercentage);
		}
		
		Picture convertedImage = ImagePictureConversion
					.convertImageToPicture(resizedImage);
		
		this.createSignboard = new SignboardCreationController(
				convertedImage.getImage(), subImageWidth, subImageHeight,
				signboardWidth, signboardHeight);

		Picture theSignboard = new Picture("Signboard",
						this.createSignboard.getSignboardImage());

		Image signboardImage = ImagePictureConversion.convertPictureToImage(theSignboard);

		this.editedImageToDisplay.setValue(signboardImage);
		
		this.setEditedImageInformation();
	}

	/**
	 * Returns a valueChangedListener for the sliders and their associated
	 * labels
	 * 
	 * @precondition None
	 * @postcondition None
	 * 
	 * @param theSlider
	 *            - the slider to get the value from
	 * @param theLabel
	 *            - the label to change
	 * 
	 * @return a valueChangedListener for the sliders and their associated
	 *         labels
	 */
	public ChangeListener<Boolean> getChangeListener(Slider theSlider, Label theLabel) {
		return new ChangeListener<Boolean>() {
			@Override
			public void changed(
							ObservableValue<? extends Boolean> observableValue,
							Boolean wasChanging, Boolean changing) {
				if (wasChanging || changing) {
					theLabel.setText((int) theSlider.getValue() + "");
				}
			}
		};
	}

	/**
	 * Creates a dialog box that collects information for resizing an image
	 * 
	 * @precondition None
	 * @postcondition None
	 * 
	 * @return the users choice for resizing the image
	 */
	public int getResizeInformation() {
		int min = integerOne;
		int max = integerOneHundred;
		int init = integerOneHundred;

		JSlider widthSlider = new JSlider(JSlider.HORIZONTAL, min, max, init);
		JSlider heightSlider = new JSlider(JSlider.HORIZONTAL, min, max, init);

		final Object[] options = { "Yes, please", "No way!" };

		final JComponent[] inputs = this.generateComponentListForResizeImage(
				widthSlider, heightSlider);

		JLabel frame = new JLabel("");
		int choice = JOptionPane.showOptionDialog(frame, inputs,
						"Resize Image", JOptionPane.YES_NO_OPTION,
						JOptionPane.PLAIN_MESSAGE, null, options, options[0]);

		if (choice == 0) {
			this.setAppropriateFieldsForResizeImage(widthSlider.getValue(), heightSlider.getValue());
		}

		return choice;
	}

	private JComponent[] generateComponentListForResizeImage(
					JSlider widthSlider, JSlider heightSlider) {
		return new JComponent[] { new JLabel("Subimage Width"), widthSlider,
			new JLabel("Subimage Hieght"), heightSlider };
	}

	private void setAppropriateFieldsForResizeImage(int subWidth, int subHeight) {

		this.resizeWidthPercentageLabelProperty.setValue("" + subWidth);
		this.widthPercentageProperty.setValue(subWidth);

		this.resizeHeightPercentageLabelProperty.setValue("" + subHeight);
		this.heightPercentageProperty.setValue(subHeight);
	}

	/**
	 * Creates a dialog box that collects information for pixelizing an image
	 * 
	 * @precondition None
	 * @postcondition None
	 * 
	 * @return the users choice
	 */
	public int getPixelizationInformation() {
		int min = integerOne;
		int max = integerOneHundred;
		int widthInit = (int) this.pixelsPerWidthProperty.getValue();
		int heightInit = (int) this.pixelsPerHeightProperty.getValue();

		JSlider subWidthSlider = new JSlider(JSlider.HORIZONTAL, min, max,
						widthInit);
		JSlider subHeightSlider = new JSlider(JSlider.HORIZONTAL, min, max,
						heightInit);

		this.setSliderProperties(subWidthSlider);
		this.setSliderProperties(subHeightSlider);

		JTextField width = new JTextField(oneHundred);
		JTextField height = new JTextField(eighty);

		final Object[] options = { "Yes, please", "No way!" };

		final JComponent[] inputs = this.generateComponentListForCreateSignboard(
					subWidthSlider, subHeightSlider, width, height);
		JLabel frame = new JLabel("");
		int choice = JOptionPane.showOptionDialog(frame, inputs,
						"Create Signboard", JOptionPane.YES_NO_OPTION,
						JOptionPane.PLAIN_MESSAGE, null, options, options[0]);

		if (choice == 0) {
			this.setAppropriateFieldsForCreateSignboard(
							subWidthSlider.getValue(), subHeightSlider.getValue(),
							width.getText(), height.getText());
		}

		return choice;
	}

	private JComponent[] generateComponentListForCreateSignboard(
					JSlider widthSlider, JSlider heightSlider, JTextField width, JTextField height) {
		
		return new JComponent[] { 
			new JLabel("Signboard Width"), width,
			new JLabel("Signboard Height"), height,
			new JLabel("Subimage Width"), widthSlider,
			new JLabel("Subimage Hieght"), heightSlider
		};
	}

	private void setSliderProperties(JSlider slider) {
		Hashtable<Integer, JLabel> labelTable = new Hashtable<Integer, JLabel>();
		labelTable.put(new Integer(integerOne), new JLabel(one));
		labelTable.put(new Integer(integerFifty), new JLabel(fifty));
		labelTable.put(new Integer(integerOneHundred), new JLabel(oneHundred));
		slider.setLabelTable(labelTable);

		slider.setPaintLabels(true);
	}

	private void setAppropriateFieldsForCreateSignboard(int subWidth,
					int subHeight, String width, String height) {
		this.signboardWidthProperty.setValue(width);
		this.signboardHeightProperty.setValue(height);

		this.pixelsPerWidthLabelProperty.setValue("" + subWidth);
		this.pixelsPerWidthProperty.setValue(subWidth);

		this.pixelsPerHeightLabelProperty.setValue("" + subHeight);
		this.pixelsPerHeightProperty.setValue(subHeight);
	}

	/**
	 * Creates a dialog box that asks the user to make a decision
	 * 
	 * @precondition None
	 * @postcondition None
	 * 
	 * @param title
	 *            - the title of the dialog box
	 * 
	 * @param message
	 *            - the question to ask in the dialog box
	 *
	 * @return the users choice
	 */
	public int replaceImageDialog(String title, String message) {
		JLabel frame = new JLabel("");

		final Object[] options = { "Yes, please", "No way!" };

		int choice = JOptionPane.showOptionDialog(frame, message, title,
						JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE, null,
						options, options[integerZero]);

		return choice;
	}

	/**
	 * Creates a dialog box that shows a message to the user
	 * 
	 * @precondition None
	 * @postcondition None
	 * 
	 * @param title
	 *            - the title of the dialog box
	 * 
	 * @param message
	 *            - the message to show in the dialog box
	 */
	public void showMessage(String title, String message) {
		JLabel frame = new JLabel("");
		JOptionPane.showMessageDialog(frame, message, title,
						JOptionPane.WARNING_MESSAGE);
	}

	/**
	 * Creates a dialog box that collects information for downloading an image
	 * 
	 * @precondition None
	 * @postcondition None
	 * 
	 * @param title
	 *            - the title of the dialog box
	 * 
	 * @param message
	 *            - the message to show in the dialog box
	 */
	public void getDownloadInformation(String title, String message) {
		JLabel frame = new JLabel("");

		String downloadLink = (String) JOptionPane.showInputDialog(frame,
						message, title, JOptionPane.YES_NO_OPTION, null, null, "");

		if (downloadLink != null) {
			this.downloadImageTextAreaProperty.setValue(downloadLink);
		}
	}
}

/**
 * 
 */
package edu.westga.Signboard.io.testPictureIO;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Test;

import edu.westga.Signboard.io.UploadPictureIO;
import edu.westga.Signboard.model.Picture;

/**
 * Tests whether getting an image from the Internet works properly
 * 
 * @author Michael Morguarge
 * @version 1.0
 */
public class WhenGetImageFromWeb {

	/**
	 * Test method for {@link edu.westga.Signboard.io.UploadPictureIO#getImageFromWeblink(java.lang.String)}.
	 */
	@Test
	public void shoudlDownloadImageFromInternetIfURLIsNotNull() {
		UploadPictureIO downloadFromInternet = UploadPictureIO.getInstance();
		Picture image = null;
		try {
			image = downloadFromInternet.getImageFromWeblink("http://2.bp.blogspot.com/_ivWX1b9wiEw/TBpEkGkdE8I/AAAAAAAAA6A/y5F5bRj57OU/s1600/bob-marley.jpg");
		} catch (IOException noImageFound) {
			noImageFound.printStackTrace();
		}
		assertEquals(false, image == null);
	}

}

/**
 * 
 */
package edu.westga.Signboard.io.testPictureIO;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;

import org.junit.Test;

import edu.westga.Signboard.io.UploadPictureIO;
import edu.westga.Signboard.model.Picture;

/**
 * Tests whether the image is loaded properly.
 * 
 * @author Michael Morguarge
 * @version 1.0
 */
public class WhenLoadingImageFromLocalComputer {

	/**
	 * Test method for {@link edu.westga.Signboard.io.PictureIO#loadImageFromLocalMachine(java.lang.String)}.
	 */
	@Test /*(expected = IllegalArgumentException.class)*/
	public void shouldLoadImageWhenFileIsNotNull() {
		Picture image = null;
		
		try {
			UploadPictureIO chooser = UploadPictureIO.getInstance();
			File file = new File("./Color Test Images/halfBlackHalfWhite.gif");
			image = chooser.loadImageFromLocalMachine(file);
		} catch (IOException imageNotFound) {
			imageNotFound.printStackTrace();
		}
		
		assertEquals(false, image == null);
	}
}

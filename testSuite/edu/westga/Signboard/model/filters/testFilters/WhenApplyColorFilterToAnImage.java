/**
 * 
 */
package edu.westga.Signboard.model.filters.testFilters;

import static org.junit.Assert.assertEquals;

import java.awt.Color;
import java.awt.image.BufferedImage;

import org.junit.Test;

import edu.westga.Signboard.model.filters.ColorFilterType;
import edu.westga.Signboard.model.filters.StandardColorFilter;

/**
 * Tests the behavior of color filtering an image
 * 
 * @author Michael Morguarge
 * @version 1.0
 */
public class WhenApplyColorFilterToAnImage {

	/**
	 * Test method for {@link edu.westga.Signboard.model.filters.StandardColorFilter#filterImage(java.awt.image.BufferedImage)}.
	 */
	@Test
	public void shouldIncreaseRedBy10WhenApplyStandardFilterOn1By1Image() {
		BufferedImage testPicture = new BufferedImage(1, 1, BufferedImage.TYPE_INT_RGB);
		
		ColorFilterType filterRed = new StandardColorFilter(testPicture, "Boost By 10", "No Filter", "No Filter");
		BufferedImage theImage = filterRed.getFilteredImage();
		Color color = new Color(theImage.getRGB(0, 0));
		
		assertEquals(10, color.getRed());
	}
	
	/**
	 * Test method for {@link edu.westga.Signboard.model.filters.StandardColorFilter#filterImage(java.awt.image.BufferedImage)}.
	 */
	@Test
	public void shouldNotIncreaseRedBy10WhenApplyStandardFilterOn1By1Image() {
		BufferedImage testPicture = new BufferedImage(1, 1, BufferedImage.TYPE_INT_RGB);
		
		Color highColor = new Color(246, 246, 246);
		testPicture.setRGB(0, 0, highColor.getRGB());
		
		ColorFilterType filterRed = new StandardColorFilter(testPicture, "Boost By 10", "No Filter", "No Filter");
		BufferedImage theImage = filterRed.getFilteredImage();
		Color color = new Color(theImage.getRGB(0, 0));
		
		assertEquals(246, color.getRed());
	}
	
	/**
	 * Test method for {@link edu.westga.Signboard.model.filters.StandardColorFilter#filterImage(java.awt.image.BufferedImage)}.
	 */
	@Test
	public void shouldIncreaseGreenBy25WhenApplyStandardFilterOn1By1Image() {
		BufferedImage testPicture = new BufferedImage(1, 1, BufferedImage.TYPE_INT_RGB);
		
		StandardColorFilter filterRed = new StandardColorFilter(testPicture, "No Filter", "Boost By 25", "No Filter");
		BufferedImage theImage = filterRed.getFilteredImage();
		Color color = new Color(theImage.getRGB(0, 0));
		
		assertEquals(25, color.getGreen());
	}
	
	/**
	 * Test method for {@link edu.westga.Signboard.model.filters.StandardColorFilter#filterImage(java.awt.image.BufferedImage)}.
	 */
	@Test
	public void shouldNotIncreaseGreenBy25WhenApplyStandardFilterOn1By1Image() {
		BufferedImage testPicture = new BufferedImage(1, 1, BufferedImage.TYPE_INT_RGB);
		
		Color highColor = new Color(231, 231, 231);
		testPicture.setRGB(0, 0, highColor.getRGB());
		
		StandardColorFilter filterRed = new StandardColorFilter(testPicture, "No Filter", "Boost By 25", "No Filter");
		BufferedImage theImage = filterRed.getFilteredImage();
		Color color = new Color(theImage.getRGB(0, 0));
		
		assertEquals(231, color.getGreen());
	}
	
	/**
	 * Test method for {@link edu.westga.Signboard.model.filters.StandardColorFilter#filterImage(java.awt.image.BufferedImage)}.
	 */
	@Test
	public void shouldIncreaseBlueBy50WhenApplyStandardFilterOn1By1Image() {
		BufferedImage testPicture = new BufferedImage(1, 1, BufferedImage.TYPE_INT_RGB);
		
		StandardColorFilter filterRed = new StandardColorFilter(testPicture, "No Filter", "No Filter", "Boost By 50");
		BufferedImage theImage = filterRed.getFilteredImage();
		Color aColor = new Color(theImage.getRGB(0, 0));
		
		assertEquals(50, aColor.getBlue());
	}
	
	/**
	 * Test method for {@link edu.westga.Signboard.model.filters.StandardColorFilter#filterImage(java.awt.image.BufferedImage)}.
	 */
	@Test
	public void shouldNotIncreaseBlueBy50WhenApplyStandardFilterOn1By1Image() {
		BufferedImage testPicture = new BufferedImage(1, 1, BufferedImage.TYPE_INT_RGB);
		
		Color highColor = new Color(206, 206, 206);
		testPicture.setRGB(0, 0, highColor.getRGB());
		
		StandardColorFilter filterRed = new StandardColorFilter(testPicture, "No Filter", "No Filter", "Boost By 50");
		BufferedImage theImage = filterRed.getFilteredImage();
		Color aColor = new Color(theImage.getRGB(0, 0));
		
		assertEquals(206, aColor.getBlue());
	}
}

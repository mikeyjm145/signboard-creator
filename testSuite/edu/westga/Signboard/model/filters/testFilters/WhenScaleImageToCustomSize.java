/**
 * 
 */
package edu.westga.Signboard.model.filters.testFilters;

import static org.junit.Assert.*;
import javafx.scene.image.Image;

import org.junit.Test;

import edu.westga.Signboard.io.ImagePictureConversion;
import edu.westga.Signboard.model.Picture;
import edu.westga.Signboard.model.controller.ResizeImageFilterController;

/**
 * Defines test cases for scaling a picture's height and width a custom percentage their value
 * 
 * @author Michael Morguarge
 * @version 1.0
 */
public class WhenScaleImageToCustomSize {
	private ResizeImageFilterController customResizeImage;
	private Picture testPicture;
	
	/**
	 * Test method for {@link edu.westga.Signboard.model.filters.ScaleToCustomSizeFilter#filterImage()}.
	 */
	@Test
	public void shouldScaleImageWidthOf20To10() {
		this.testPicture = new Picture("test", 10, 20);
		this.customResizeImage = new ResizeImageFilterController(ImagePictureConversion.convertPictureToImage(this.testPicture));
		Image thePicture = this.customResizeImage.resizePictureCustomSize(1.0, 0.5);
		Picture customResizedPicture = ImagePictureConversion.convertImageToPicture(thePicture);
		assertEquals(10, customResizedPicture.getWidth());
	}
	
	/**
	 * Test method for {@link edu.westga.Signboard.model.filters.ScaleToCustomSizeFilter#filterImage()}.
	 */
	@Test
	public void shouldScaleImageHeightOf10To10() {
		this.testPicture = new Picture("test", 20, 10);
		this.customResizeImage = new ResizeImageFilterController(ImagePictureConversion.convertPictureToImage(this.testPicture));
		Image thePicture = this.customResizeImage.resizePictureCustomSize(.5, 1);
		Picture customResizedPicture = ImagePictureConversion.convertImageToPicture(thePicture);
		assertEquals(10, customResizedPicture.getHeight());
	}
	
	/**
	 * Test method for {@link edu.westga.Signboard.model.filters.ScaleToCustomSizeFilter#filterImage()}.
	 */
	@Test
	public void shouldScaleImageWidthOf11To6() {
		this.testPicture = new Picture("test", 11, 10);
		this.customResizeImage = new ResizeImageFilterController(ImagePictureConversion.convertPictureToImage(this.testPicture));
		Image thePicture = this.customResizeImage.resizePictureCustomSize(.5, 1);
		Picture customResizedPicture = ImagePictureConversion.convertImageToPicture(thePicture);
		assertEquals(6, customResizedPicture.getWidth());
	}
	
	/**
	 * Test method for {@link edu.westga.Signboard.model.filters.ScaleToCustomSizeFilter#filterImage()}.
	 */
	@Test
	public void shouldScaleImageWidthOf1To1() {
		this.testPicture = new Picture("test", 1, 10);
		this.customResizeImage = new ResizeImageFilterController(ImagePictureConversion.convertPictureToImage(this.testPicture));
		Image thePicture = this.customResizeImage.resizePictureCustomSize(.5, 1);
		Picture customResizedPicture = ImagePictureConversion.convertImageToPicture(thePicture);
		assertEquals(1, customResizedPicture.getWidth());
	}
}

/**
 * 
 */
package edu.westga.Signboard.model.filters.testFilters;

import static org.junit.Assert.*;
import javafx.scene.image.Image;

import org.junit.Test;

import edu.westga.Signboard.io.ImagePictureConversion;
import edu.westga.Signboard.model.Picture;
import edu.westga.Signboard.model.controller.ResizeImageFilterController;

/**
 * Defines test cases for scaling a picture's height and width to half their value
 * 
 * @author Michael Morguarge
 * @version 1.0
 */
public class WhenScaleToHalfSize {

	private ResizeImageFilterController resizeImage;
	private Picture testPicture;
	
	/**
	 * Test method for {@link edu.westga.pictureeditor.model.Picture#scaleToHalfSize()}.
	 */
	@Test
	public final void shouldScaleHeightFrom10To5() {
		this.testPicture = new Picture("name", 10, 10);
		this.resizeImage = new ResizeImageFilterController(ImagePictureConversion.convertPictureToImage(this.testPicture));
		Image thePicture = this.resizeImage.resizePictureHalfSize();
		Picture halfScaledHeightPicture = ImagePictureConversion.convertImageToPicture(thePicture);
		assertEquals(5, halfScaledHeightPicture.getHeight());
		this.testPicture = null;
	}
	
	/**
	 * Test method for {@link edu.westga.pictureeditor.model.Picture#scaleToHalfSize()}.
	 */
	@Test
	public final void shouldScaleHeightFrom11To6() {
		this.testPicture = new Picture("name", 11, 11);
		this.resizeImage = new ResizeImageFilterController(ImagePictureConversion.convertPictureToImage(this.testPicture));
		Image thePicture = this.resizeImage.resizePictureHalfSize();
		Picture halfScaledHeightPicture = ImagePictureConversion.convertImageToPicture(thePicture);
		assertEquals(6, halfScaledHeightPicture.getHeight());
		this.testPicture = null;
	}
	
	/**
	 * Test method for {@link edu.westga.pictureeditor.model.Picture#scaleToHalfSize()}.
	 */
	@Test
	public final void shouldScaleHeightFrom1To1() {
		this.testPicture = new Picture("name", 1, 1);
		this.resizeImage = new ResizeImageFilterController(ImagePictureConversion.convertPictureToImage(this.testPicture));
		Image thePicture = this.resizeImage.resizePictureHalfSize();
		Picture halfScaledHeightPicture = ImagePictureConversion.convertImageToPicture(thePicture);
		assertEquals(1, halfScaledHeightPicture.getHeight());
		this.testPicture = null;
	}
	

	/**
	 * Test method for {@link edu.westga.pictureeditor.model.Picture#scaleToHalfSize()}.
	 */
	@Test
	public final void shouldScaleWidthFrom10To5() {
		this.testPicture = new Picture("name", 10, 10);
		this.resizeImage = new ResizeImageFilterController(ImagePictureConversion.convertPictureToImage(this.testPicture));
		Image thePicture = this.resizeImage.resizePictureHalfSize();
		Picture halfScaledHeightPicture = ImagePictureConversion.convertImageToPicture(thePicture);
		assertEquals(5, halfScaledHeightPicture.getWidth());
		this.testPicture = null;
	}
	
	/**
	 * Test method for {@link edu.westga.pictureeditor.model.Picture#scaleToHalfSize()}.
	 */
	@Test
	public final void shouldScaleWidthFrom11To6() {
		this.testPicture = new Picture("name", 11, 11);
		this.resizeImage = new ResizeImageFilterController(ImagePictureConversion.convertPictureToImage(this.testPicture));
		Image thePicture = this.resizeImage.resizePictureHalfSize();
		Picture halfScaledHeightPicture = ImagePictureConversion.convertImageToPicture(thePicture);
		assertEquals(6, halfScaledHeightPicture.getWidth());
		this.testPicture = null;
	}
	
	/**
	 * Test method for {@link edu.westga.pictureeditor.model.Picture#scaleToHalfSize()}.
	 */
	@Test
	public final void shouldScaleWidthFrom1To1() {
		this.testPicture = new Picture("name", 1, 1);
		this.resizeImage = new ResizeImageFilterController(ImagePictureConversion.convertPictureToImage(this.testPicture));
		Image thePicture = this.resizeImage.resizePictureHalfSize();
		Picture halfScaledHeightPicture = ImagePictureConversion.convertImageToPicture(thePicture);
		assertEquals(1, halfScaledHeightPicture.getWidth());
		this.testPicture = null;
	}

}

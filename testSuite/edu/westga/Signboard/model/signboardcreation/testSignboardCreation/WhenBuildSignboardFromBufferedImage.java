/**
 * 
 */
package edu.westga.Signboard.model.signboardcreation.testSignboardCreation;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import org.junit.Test;

import edu.westga.Signboard.io.UploadPictureIO;
import edu.westga.Signboard.model.Picture;
import edu.westga.Signboard.model.signboardcreation.SignboardBuilder;

/**
 * Tests the behavior of creating a signboard image.
 * 
 * @author Michael Morguarge
 * @version 1.0
 */
public class WhenBuildSignboardFromBufferedImage {

	/**
	 * Test behavior of pixelating a picture with 1 by 1 subimages
	 * {@link edu.westga.Signboard.model.signboardcreation.SignboardBuilder#getSignboardImage()}
	 * .
	 */
	@Test
	public void shouldCopyAndPixelizeImageIfSignboardDimensionsAre0By0AndSubimageIs4By4() {
		final BufferedImage testPicture = this.uploadPicture().getImage();
		
		SignboardBuilder build = new SignboardBuilder(testPicture, 4, 4, 0, 0);
		
		BufferedImage theImage = build.getSignboardImage();
		
		BufferedImage[] theImages = {testPicture, theImage};
		this.showImage("The Pixelated Signboard Image",
						"This is the original image",
						"This is the pixalated signboard image",
						theImages);
	}

	/**
	 * Test behavior of pixelating a picture with 2 by 2 subimages
	 * {@link edu.westga.Signboard.model.signboardcreation.SignboardBuilder#getSignboardImage()}
	 * .
	 */
	@Test
	public void shouldCreatePixelatedSignboardImageIfSignboardDimensionsAre100By80AndSubimageIs2By2() {
		BufferedImage testPicture = this.uploadPicture().getImage();
		
		SignboardBuilder build = new SignboardBuilder(testPicture, 2, 2,
						100, 80);
		
		BufferedImage theImage = build.getSignboardImage();
		
		BufferedImage[] theImages = {testPicture, theImage};
		this.showImage("The Pixelated Signboard Image",
						"This is the original image",
						"This is the pixalated signboard image",
						theImages);
	}

	private void showImage(String title, String message1, String message2, BufferedImage[] theImages) {
		JLabel frame = new JLabel("");
		Icon icon = new ImageIcon(theImages[0]);
		JLabel image = new JLabel();
		image.setIcon(icon);
		Icon icon2 = new ImageIcon(theImages[1]);
		JLabel image2 = new JLabel();
		image2.setIcon(icon2);

		JComponent[] components = new JComponent[] { new JLabel(message1), image, new JLabel(message2), image2 };

		JOptionPane.showOptionDialog(
						frame,
						components,
						title,
						JOptionPane.CLOSED_OPTION,
						JOptionPane.ERROR_MESSAGE,
						null, null, null);
	}

	private Picture uploadPicture() {
		Picture image = null;
		try {
			UploadPictureIO chooser = UploadPictureIO.getInstance();
			File file = new File("./Color Test Images/colorful.jpg");
			image = chooser.loadImageFromLocalMachine(file);
		} catch (IOException imageNotFound) {
			imageNotFound.printStackTrace();
		}

		return image;
	}

}

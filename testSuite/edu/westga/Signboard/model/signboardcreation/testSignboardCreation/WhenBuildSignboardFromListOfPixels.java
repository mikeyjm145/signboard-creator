
package edu.westga.Signboard.model.signboardcreation.testSignboardCreation;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import org.junit.Test;

import edu.westga.Signboard.io.UploadPictureIO;
import edu.westga.Signboard.model.Picture;
import edu.westga.Signboard.model.signboardcreation.SignboardBuilderUsingPixelList;
import edu.westga.Signboard.model.signboardcreation.SignboardPixelFactory;

/**
 * Tests the behavior of creating a signboard image from a list of pixels
 * 
 * @author Michael Morguarge
 * @version 1.0
 */
public class WhenBuildSignboardFromListOfPixels {

	/**
	 * Test behavior of pixelating a picture with 1 by 1 subimages
	 * Test method for {@link edu.westga.Signboard.model.signboardcreation.SignboardBuilder#getSignboardImage()}.
	 */
	@Test
	public void shouldCreateAndCopyImageIfSubimageIs1By1() {
		BufferedImage testPicture = this.getImage().getImage();
		SignboardPixelFactory convertToPixels = new SignboardPixelFactory();
		
		ArrayList<Color> colors = convertToPixels.convertImageToPixels(testPicture, 1, 1);
		
		SignboardBuilderUsingPixelList build = new SignboardBuilderUsingPixelList(colors, testPicture.getWidth(), testPicture.getHeight(), 1, 1, 0, 0);
		
		BufferedImage theImage = build.getSignboardImage();
		
		BufferedImage[] theImages = {testPicture, theImage};
		this.showImage("The Pixelated Signboard Image",
						"This is the original image",
						"This is the rebuilt image",
						theImages);
	}
	
	/**
	 * Test behavior of pixelating a picture with 1 by 1 subimages
	 * Test method for {@link edu.westga.Signboard.model.signboardcreation.SignboardBuilder#getSignboardImage()}.
	 */
	@Test
	public void shouldCreateSignboardImageFromListOfPixelsIfSubimageIs4By4() {
		BufferedImage testPicture = this.getImage().getImage();
		SignboardPixelFactory convertToPixels = new SignboardPixelFactory();
		
		ArrayList<Color> colors = convertToPixels.convertImageToPixels(testPicture, 1, 1);
		
		SignboardBuilderUsingPixelList build = new SignboardBuilderUsingPixelList(colors, testPicture.getWidth(), testPicture.getHeight(), 4, 4, 200, 200);
		
		BufferedImage theImage = build.getSignboardImage();
		
		BufferedImage[] theImages = {testPicture, theImage};
		this.showImage("The Pixelated Signboard Image",
						"This is the original image",
						"This is the rebuilt image",
						theImages);
	}
	
	private void showImage(String title, String message1, String message2, BufferedImage[] theImages) {
		JLabel frame = new JLabel("");
		Icon icon = new ImageIcon(theImages[0]);
		JLabel image = new JLabel();
		image.setIcon(icon);
		Icon icon2 = new ImageIcon(theImages[1]);
		JLabel image2 = new JLabel();
		image2.setIcon(icon2);

		JComponent[] components = new JComponent[] { new JLabel(message1), image, new JLabel(message2), image2 };

		JOptionPane.showOptionDialog(
						frame,
						components,
						title,
						JOptionPane.CLOSED_OPTION,
						JOptionPane.ERROR_MESSAGE,
						null, null, null);
	}

	private Picture getImage() {
		Picture image = null;
		try {
			UploadPictureIO chooser = UploadPictureIO.getInstance();
			File file = new File("./Color Test Images/halfBlackHalfWhite.gif");
			image = chooser.loadImageFromLocalMachine(file);
		} catch (IOException imageNotFound) {
			imageNotFound.printStackTrace();
		}

		return image;
	}

}

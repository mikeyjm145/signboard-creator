/**
 * 
 */
package edu.westga.Signboard.model.signboardcreation.testSignboardCreation;

import static org.junit.Assert.*;

import java.awt.Color;
import java.awt.image.BufferedImage;

import org.junit.Test;

import edu.westga.Signboard.model.signboardcreation.SignboardPixelFactory;

/**
 * Tests whether the PixelCreator class correctly creates a picture
 * 
 * @author Michael Morguarge
 * @version 1.0
 */
public class WhenCreateSignboardPixel {
	
	private SignboardPixelFactory pixelFactory;

	/**
	 * Test method for
	 * {@link edu.westga.Signboard.model.SignboardPixelFactory#getColorFromSubimage(java.awt.image.BufferedImage)}
	 */
	@Test
	public void shouldCreateAPixelFromAOneByOneBufferedImage() {
		BufferedImage oneByOne = this.createMultiColoredImage(1, 1);
		
		this.pixelFactory = new SignboardPixelFactory();
		Color signboardPixel = this.pixelFactory.getAverageColorFromSubimage(oneByOne);

		assertEquals(new Color(0, 0, 0), signboardPixel);
	}
	
	/**
	 * Test method for
	 * {@link edu.westga.Signboard.model.SignboardPixelFactory#getColorFromSubimage(java.awt.image.BufferedImage)}
	 */
	@Test
	public void shouldCreateAPixelFromAFiveByFiveBufferedImage() {
		BufferedImage fiveByFive = this.createMultiColoredImage(5, 5);
		
		this.pixelFactory = new SignboardPixelFactory();
		Color signboardPixel = this.pixelFactory.getAverageColorFromSubimage(fiveByFive);
		
		assertEquals(new Color(40, 40, 40), signboardPixel);
	}
	
	/**
	 * Test method for
	 * {@link edu.westga.Signboard.model.SignboardPixelFactory#getColorFromSubimage(java.awt.image.BufferedImage)}
	 */
	@Test
	public void shouldCreateAPixelFromAFiveByTwentyBufferedImage() {
		BufferedImage fiveByTwenty = this.createMultiColoredImage(5, 20);

		this.pixelFactory = new SignboardPixelFactory();
		Color signboardPixel = this.pixelFactory.getAverageColorFromSubimage(fiveByTwenty);
		
		assertEquals(new Color(115, 115, 115), signboardPixel);
	}

	private BufferedImage createMultiColoredImage(int width, int height) {
		BufferedImage newImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		for (int row = 0; row < width; row++) {
			for (int col = 0; col < height; col++) {
				int aColorValue = (((((row + col) * 10)) % 256));
				Color pixelValue = new Color(aColorValue, aColorValue, aColorValue);
				newImage.setRGB(row, col, pixelValue.getRGB());
			}
		}
		return newImage;
	}
}

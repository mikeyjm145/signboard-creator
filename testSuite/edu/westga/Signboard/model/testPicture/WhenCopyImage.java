/**
 * 
 */
package edu.westga.Signboard.model.testPicture;

import org.junit.Before;
import org.junit.Test;

import edu.westga.Signboard.model.Picture;

/**
 * Defines the test case for copying a Picture's image.
 * 
 * @author Michael Morguarge
 * @version 1.0
 */
public class WhenCopyImage {
	
	private Picture testPicture;

	/**
	 * Sets up data for testing
	 * @throws java.lang.Exception - if the setup fails
	 */
	@Before
	public void setUp() throws Exception {
		
	}

	/**
	 * Test method for {@link edu.westga.pictureeditor.model.Picture#getImage()}.
	 */
	@Test
	public void shouldRunFast() {
		this.testPicture = new Picture("Name", 500, 500);
		long startTime = 0;
		long endTime = 0;
		int totalTime = 0;
		for (int i = 0; i < 1000; i++) {
			startTime = System.nanoTime();
			this.testPicture.getImage();
			endTime = System.nanoTime();
			totalTime += (int) ((endTime - startTime) / 1000000);
		}
		
		System.out.println("Average Time: " + totalTime);
	}

}
